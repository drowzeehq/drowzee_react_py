import json
from exceptions.ConnectionInterrupted import ConnectionInterrupted
from exceptions.DeviceNotConnectedException import DeviceNotConnectedException
from exceptions.MobileDeviceNotSupportedException import MobileDeviceNotSupportedException
from utils.detectors.OpenedEyesDetector import OpenedEyesDetector
import asyncio
import time
import logging

class AbstractModule:
    '''
    Parent class for all 3 modules
    '''

    def __init__(self, ctx, conf):
        '''

        :param ctx: app context
        :param conf: app configurations
        :param update_data_callback: function to be called every epoch
        :param module_finished_callback: function to be called when the module is finished
        :param on_error_callback: function to be called in case of an error
        '''
        self.ctx = ctx
        self.conf = conf

        self.eeg_source = ctx.eeg_source
        self.device_on_head_detector = ctx.device_on_head_detector
        self.noise_detector = ctx.noise_detector
        self.session_recorder = ctx.session_recorder
        self.opened_eyes_detector = OpenedEyesDetector(conf)
        self.save_data_method = None
        self.protocol = ctx.protocol
        self.react_py_tunnel = None
        self.end_module = False

    async def on_error_callback(self, text):
        event = json.dumps({'name': "error", 'text': text})
        await self.react_py_tunnel.websocket.send(event.encode())

    def perform_loop(self, react_py_tunnel, perform):
        while self.end_module == False:
            self.perform_logic(react_py_tunnel, perform)
            time.sleep(0.2)
        self.end_module = False

    def perform_logic(self, react_py_tunnel, perform):
        '''
            The main module method, calls each subclass perform and handles errors if there are any
        '''
        loops = asyncio.new_event_loop()
        asyncio.set_event_loop(loops)
        self.react_py_tunnel = react_py_tunnel
        try:
            loops.run_until_complete(perform())

        except DeviceNotConnectedException:
            logging.error('Device not connected exception')
            self._end_session_with_error(loops, text="Can't connect.\nMake sure the device is turned on")

        except MobileDeviceNotSupportedException:
            logging.error('Device not supported exception')
            self._end_session_with_error(loops, text="Unfortunately, your phone\nmodel is not supported")

        except ConnectionInterrupted as e:
            logging.error(e)
            self._end_session_with_error(loops, text=e.message )

        loops.close()

    def perform(self):
        '''
        Main logic of each module. It is implemented in each subclass module
        :return:
        '''
        raise NotImplementedError

    def _check_device_connectivity(self, timestamp, epoch):
        '''
        Checks every epoch if device is on head, whether there is a bluetooth connection
        :param timestamp:
        :param epoch:
        :return:
        '''
        self._check_device_on_head(epoch)
        self._check_bluetooth()

    def _check_device_on_head(self, epoch):
        '''
        Check if the headband is on head or not. In case if not ends session with error window
        :param epoch: epoch to analyse data on
        '''
        if not self.device_on_head_detector.detect(epoch) and not self.conf.auto_run:
            self._end_session_with_error(text='Device was displaced')

    def _check_bluetooth(self):
        '''
        Check if the bluetooth is connected. In case if not ends session with error window
        '''
        self.ctx.eeg_source.check_bluetooth()
        if self.ctx.eeg_source.bluetooth_enabled == False:
            self._end_session_with_error(text='Bluetooth disabled')

    def _gather_epoch_data(self):
        '''
        Method that is called in the beginnig of Calibration and Training modules. It extracts the next session
        from eeg_source and analyses if there is a noise
        :param noise_sound_enabled: boolean flag that says if noise sound should be played
        :param noise_volume: volume of the noise sound
        :return:
        '''
        epoch, timestamp = self.eeg_source.next_epoch()
        self._check_device_connectivity(timestamp, epoch)

        is_signal_noisy = self.noise_detector.detect(epoch)
        is_eyes_opened = self.opened_eyes_detector.detect(epoch)

        is_eyes_opened *= self.conf.eye_detector_enabled

        return epoch, timestamp, is_signal_noisy, is_eyes_opened

    def end_session(self):
        '''
        Handling the ending of the session by closing the connection with headband, stopping all the music,
        and saving data to the file
        '''
        self.ctx.session_data.last_timestamp = None
        self.eeg_source.disconnect()
        self.ctx.noise_detector.clear_buffer()
        self.save_data_method(self.ctx.session_data)


    def _end_session_with_error(self, loop , text):
        '''
        Handling the ending of the session in case of an error. Shows error window
        :param text: text to show in the error window
        '''
        self.end_session()
        loop.run_until_complete(self.on_error_callback(text))
        loop.close()
