import numpy as np
import json
import asyncio
import logging
from modules.AbstractModule import AbstractModule


class DevicePlacingModule(AbstractModule):
    '''
    Class for performing the Device Placing Module logic
    '''
    def __init__(self, ctx, conf):
        super(DevicePlacingModule, self).__init__(ctx, conf)
        '''
            :param ctx: app context
            :param conf: app configuration
        '''
        self.save_data_method = self.session_recorder.save_device_placing_data
        self.channel_names = self.eeg_source.channel_names
        self.n_channels = conf.n_channels

        # For each epoch the expected signal quality metrics are [1, 1, 1, 1] (for 4 electrodes). Values
        # are used as constant vales and are not modified anywhere
        self.ideal_signal_quality = np.ones(self.n_channels)

        # Buffer for each epoch to contain each signal's signal quality boolean metrics
        # (0 = bad quality, 1 = good quality)
        self.signal_quality = np.zeros(self.n_channels)

        # Counts sequential clean epochs (this is the value of progress bar on the Device Placing Screen)
        # If at least 1 electrode has noise, it will be reset to 0. The counter counts only when all electrodes show
        # [1, 1, 1, 1]
        self.stabilization_counter = 0
        self.progress_bar_value = 0

    async def perform(self):
        '''
        Main logic for the Device Placing module
        '''
        logging.info("===============================DEV PLACING======================================")
        epoch, timestamp = self.eeg_source.next_epoch_check_nan()
        logging.info('Epoch mean: {}'.format(np.mean(epoch, axis=0)))
        self._calculate_signal_quality(epoch)
        self.session_recorder.store_device_placing_data(self.ctx.session_data, epoch, timestamp, self.signal_quality)
        self._print_signal_quality(self.signal_quality)
        if not np.array_equal(self.signal_quality, self.ideal_signal_quality) and not self.conf.auto_run:
            self.stabilization_counter = 0
            await self.send_data()

        else:
            self.stabilization_counter += 1
            await self.send_data()
            if self.stabilization_counter > self.conf.device_placing_stabilization_length:
                self.session_recorder.save_device_placing_data(self.ctx.session_data)
                logging.info("Device placing module end")
                await self.module_finished_callback()

    def _calculate_signal_quality(self, epoch):
        self.signal_quality = self.device_on_head_detector.detect_per_channel(epoch)
        if self.conf.device_placing_noise_detection_enabled:
            is_signal_not_noisy = np.logical_not(self.noise_detector.detect_per_electrode(epoch))
            self.signal_quality = self.signal_quality * is_signal_not_noisy

    def _print_signal_quality(self, signal_quality):
        result = "| "
        for i_channel in range(self.n_channels):
            result += self.channel_names[i_channel] + " : " + str(signal_quality[i_channel]) + " | "
        print(result)
        logging.debug(result)

    async def send_data(self):
        self.progress_bar_value = int(self.stabilization_counter * 100 / self.conf.device_placing_stabilization_length)
        logging.debug('Data sent : name : device_placing_data, quality : {}'.format(self.signal_quality.tolist()))
        event = json.dumps({'name': 'device_placing_data', 'quality': self.signal_quality.tolist(),
                            'progress_bar': self.progress_bar_value})
        await self.react_py_tunnel.websocket.send(event)

    async def module_finished_callback(self):
        self.end_module= True
        event = json.dumps({'name': 'device_placing_end'})
        await self.react_py_tunnel.websocket.send(event)
        self.initModule()

    def initModule(self):
        self.signal_quality = np.zeros(self.n_channels)
        self.stabilization_counter = 0
        self.progress_bar_value = 0



