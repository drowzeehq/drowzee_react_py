from collections import deque

import numpy as np
import json
import logging 

from dto.EpochData import TrainingEpochData
from modules.AbstractModule import AbstractModule


class TrainingModule(AbstractModule):
    '''
    Class for performing the Training Module logic
    '''

    def __init__(self, ctx, conf):
        super(TrainingModule, self).__init__(ctx, conf)
        '''
            :param ctx: app context
            :param conf: app configuration
        '''
        self.save_data_method = self.session_recorder.save_training_data
        self.bands_transformer = ctx.bands_transformer

        # Contains number of epochs based on epoch length, duration and overlap between epochs
        self.session_duration = int(self.conf.training_length * (
                self.conf.epoch_length / (self.conf.epoch_length - self.conf.overlap_length)))

        self.training_thresholds = None

        # For each epoch protocol scores are calculated. They are used in the same way as calibration_mean_powers,
        # except calibration_mean_powers used only during first 90 seconds of training session,
        # and self.protocol_scores_array is used during the rest of the training session. Each 90 seconds they are
        # being calculated
        self.protocol_scores_array_size = int(90 * (self.conf.epoch_length / self.conf.shift_length))
        logging.debug('self.protocol_scores_array_size: {}'.format(self.protocol_scores_array_size))
        print('self.protocol_scores_array_size ', self.protocol_scores_array_size)
        self.protocol_scores_array = np.zeros((self.protocol_scores_array_size, self.conf.n_channels))

        # Queue to contain noise binary values (1 = there was noise) about last 4 epochs
        self.noisy_epoch_deque = deque(maxlen=int(4 * self.conf.epoch_length / self.conf.shift_length))

        # Number of passed epochs
        self.nb_epochs = 0
        self.current_training_epoch_data = TrainingEpochData(self.session_duration)
        self.progress_bar_value = 0

    async def perform(self, *args):
        '''
        Main logic of the training module
        :param args: ignored
        '''
        logging.info("====================================TRAINING=============================")

        finished = self.nb_epochs >= self.session_duration
        if not finished:
            epoch, timestamp, display_signal_noisy, is_eyes_opened = self._gather_epoch_data()

            self.noisy_epoch_deque.append(display_signal_noisy)
            is_signal_noisy = sum(self.noisy_epoch_deque) != 0

            self.protocol.perform_training(self.ctx, is_signal_noisy, epoch,
                                           self.protocol_scores_array)
            self.training_thresholds = self.protocol.training_thresholds
            self.protocol_scores_array = self.protocol.protocol_scores_array

            self._populate_training_epoch_data(display_signal_noisy, is_eyes_opened, self.protocol.should_play_feedback)
            #self.update_data_callback(self.current_training_epoch_data)

            self.store_training_data(epoch, timestamp, display_signal_noisy, is_eyes_opened,
                                     self.protocol.should_play_feedback, self.protocol.protocol_scores_mean)

            self.nb_epochs += 1
            self.update_scores(self.protocol.should_play_feedback, display_signal_noisy)
            await self.send_data()

        else:
            # session is finished!
            self.session_recorder.save_training_data(self.ctx.session_data)
            self.ctx.noise_detector.clear_buffer()
            self.ctx.daily_sessions_restriction_handler.update_daily_sessions_counter()
            self.ctx.session_data.noise_score = self.nb_epochs - self.ctx.session_data.noise_score
            logging.info("Training module end")
            await self.module_finished_callback()

    def _populate_training_epoch_data(self, is_signal_noisy, is_eyes_opened, should_play_feedback):
        self.current_training_epoch_data.is_signal_noisy = is_signal_noisy
        self.current_training_epoch_data.is_eyes_opened = is_eyes_opened
        self.current_training_epoch_data.is_neurofeedback = int(should_play_feedback)
        self.current_training_epoch_data.n_epochs_collected = self.nb_epochs

    def store_training_data(self, epoch, timestamp, is_signal_noisy, is_eyes_opened, should_play_feedback,
                            protocol_scores_mean):
        '''
            Store epoch raw end metrics data into session buffers
        '''
        metrics = [timestamp, is_signal_noisy, is_eyes_opened, should_play_feedback, protocol_scores_mean]
        self.session_recorder.store_training_data(self.ctx.session_data, epoch, timestamp, metrics)

    def update_scores(self, should_play_feedback, display_signal_noisy):
        '''
            Updates training performance score and signal quality score
        '''
        if should_play_feedback:
            self.ctx.session_data.training_score += 1
        if display_signal_noisy:
            self.ctx.session_data.noise_score += 1

    async def send_data(self):
        self.progress_bar_value = int(self.current_training_epoch_data.n_epochs_collected * 100 /
                                      self.current_training_epoch_data.n_epochs_total)
        logging.debug('Data sent : name : training_data, is_signal_noisy : {0}, is_neurofeedback : {1}, progressbar : {2}'.format(self.current_training_epoch_data.is_signal_noisy,
                                                                                                                                  self.current_training_epoch_data.is_neurofeedback,
                                                                                                                                  self.progress_bar_value))
        event = json.dumps({'name': 'training_data',
                            'is_signal_noisy': self.current_training_epoch_data.is_signal_noisy,
                            'is_neurofeedback': self.current_training_epoch_data.is_neurofeedback,
                            'progress_bar': self.progress_bar_value})
        await self.react_py_tunnel.websocket.send(event)

    async def module_finished_callback(self):
        self.end_module= True
        event = json.dumps({"name": "training_end"})
        await self.react_py_tunnel.websocket.send(event)
        self.initModule()

    def initModule(self):
        self.training_thresholds = None
        self.protocol_scores_array = np.zeros((self.protocol_scores_array_size, self.conf.n_channels))
        self.noisy_epoch_deque = deque(maxlen=int(4 * self.conf.epoch_length / self.conf.shift_length))
        self.nb_epochs = 0
        self.current_training_epoch_data = TrainingEpochData(self.session_duration)
        self.progress_bar_value = 0