from collections import deque
import json
import logging

from dto.EpochData import CalibrationEpochData
from modules.AbstractModule import AbstractModule
from utils.detectors.OpenedEyesDetector import OpenedEyesDetector
from modules.TrainingModule import TrainingModule


class CalibrationModule(AbstractModule):
    '''
    Class for performing the Calibration Module logic
    '''

    def __init__(self, ctx, conf,modules):
        '''

        :param ctx: app context
        :param conf: app configuration
        :param update_data_callback: function to be called every epoch
        :param module_finished_callback: function to be called when the module is finished
        :param on_error_callback: function to be called in case of an error
        '''
        super(CalibrationModule, self).__init__(ctx, conf)
        self.save_data_method = self.session_recorder.save_calibration_data
        self.opened_eyes_detector = OpenedEyesDetector(conf)
        self.bands_transformer = ctx.bands_transformer
        self.n_channels = conf.n_channels
        self.eyes_opened_epochs_deque = deque(maxlen=5)
        self.modules = modules
        # number of calibration epochs calculated based on the epoch length, calibration length in seconds and the
        # overlap between epochs
        self.n_calibration_epochs = int(
            self.conf.calibration_length * self.conf.epoch_length / self.conf.shift_length)

        self.calibration_epochs = []

        # Object that contains epoch metrics
        self.current_calibration_epoch_data = CalibrationEpochData(self.n_calibration_epochs)

        # Maximum number of epochs in a queue based on clea epoch ratio (which is currently = 85%)
        self.max_epochs_in_deque = int(self.n_calibration_epochs * (2 - self.conf.clean_epochs_ratio))

        # Array of the last 'max_epochs_in_deque' epochs that contains boolean values for whether the epoch is clean
        # or not
        self.state_last_epochs_deque = deque(maxlen=self.max_epochs_in_deque)
        # Array of the last 'max_epochs_in_deque' epochs that contains the epochs raw EEG data
        self.last_epochs_deque = deque(maxlen=self.max_epochs_in_deque)

        # Contains boolean values for last epochs noise. The array is used to not continue calibration progress if all
        # 2 last epochs are noisy
        self.noisy_epoch_deque = deque(maxlen=int(2 * self.conf.epoch_length / self.conf.shift_length))

        # Number of current clean epochs
        self.n_current_clean_epochs = 0
        self.progress_bar_value = 0

    async def perform(self, *args):
        '''
        The main logic of the module
        :param args: not used, ignore
        '''
        logging.info("==================================CALIB===================================")
        if self.n_current_clean_epochs < self.n_calibration_epochs:
            epoch, timestamp, is_signal_noisy, is_eyes_opened = self._gather_epoch_data()
            self.session_recorder.store_calibration_data(self.ctx.session_data, epoch, timestamp,
                                                         [timestamp, is_signal_noisy, is_eyes_opened])

            self.eyes_opened_epochs_deque.append(is_eyes_opened)
            self.last_epochs_deque.append(epoch)
            is_signal_clean = not (is_signal_noisy or is_eyes_opened)
            self.noisy_epoch_deque.append(not is_signal_clean)
            self.state_last_epochs_deque.append(sum(self.noisy_epoch_deque) == 0 or self.conf.auto_run)

            self.n_current_clean_epochs = self.state_last_epochs_deque.count(1)
            current_clean_epochs_ratio = self.n_current_clean_epochs / self.n_calibration_epochs

            self.populate_calibration_epoch_data(is_signal_noisy, is_eyes_opened, current_clean_epochs_ratio)
            self.progress_bar_value = int(self.current_calibration_epoch_data.n_epochs_collected * 100 /
                                          self.current_calibration_epoch_data.n_epochs_total)
            await self.send_data(is_signal_noisy)
        else:
            self._gather_clean_epochs()
            self.protocol.perform_calibration(self.ctx, self.calibration_epochs, self.n_calibration_epochs)
            calibration_thresholds = self.protocol.calibration_thresholds
            self.ctx.session_data.calibration_mean_powers = calibration_thresholds
            self.session_recorder.save_calibration_data(self.ctx.session_data)
            logging.info('Calibration is done')
            logging.debug('Calibration thresholds: {}'.format(calibration_thresholds))
            await self.module_finished_callback()

    def populate_calibration_epoch_data(self, is_signal_noisy, is_eyes_opened,
                                        clean_epochs_ratio):
        self.current_calibration_epoch_data.is_signal_noisy = is_signal_noisy
        self.current_calibration_epoch_data.is_eyes_opened = is_eyes_opened
        self.current_calibration_epoch_data.n_epochs_collected = self.n_current_clean_epochs
        self.current_calibration_epoch_data.clean_epochs_ratio = clean_epochs_ratio

    def _print_epoch_metrics(self, epoch_number, is_signal_noisy, is_eyes_opened,
                             current_ratio):
        print('\nEpoch #{0}:'.format(epoch_number))
        print('N={0} E={1}, nEpochs={2}/{3}'.format(
            is_signal_noisy,
            is_eyes_opened,
            self.n_current_clean_epochs,
            self.n_calibration_epochs), end='')
        if self.n_current_clean_epochs != 0:
            print(' R={0}%'.format(current_ratio * 100), end='')
        print()

    def _gather_clean_epochs(self):
        '''
        Get only clean epochs from the self.last_epochs_deque (by defal there will be 85% of clean epochs)
        '''
        while len(self.state_last_epochs_deque) != 0:
            last_epoch = self.last_epochs_deque.popleft()
            if self.state_last_epochs_deque.popleft() == 1:
                self.calibration_epochs.append(last_epoch)

    async def send_data(self,is_signal_noisy):
        logging.debug('Data sent : name: calibration_data, is_signal_noisy: {0}, progress_bar: {1}'.format(is_signal_noisy,self.progress_bar_value))
        event = json.dumps({'name': 'calibration_data', 'is_signal_noisy': is_signal_noisy,
                            'progress_bar': self.progress_bar_value})
        await self.react_py_tunnel.websocket.send(event)

    async def module_finished_callback(self):
        self.end_module= True
        event = json.dumps({'name': 'calibration_end'})
        await self.react_py_tunnel.websocket.send(event)
        self.initModule()

    def initModule(self):
        self.calibration_epochs = []
        self.current_calibration_epoch_data = CalibrationEpochData(self.n_calibration_epochs)
        self.state_last_epochs_deque = deque(maxlen=self.max_epochs_in_deque)
        self.last_epochs_deque = deque(maxlen=self.max_epochs_in_deque)
        self.noisy_epoch_deque = deque(maxlen=int(2 * self.conf.epoch_length / self.conf.shift_length))
        self.n_current_clean_epochs = 0
        self.progress_bar_value = 0





