from MainApp import MainApp
from utils.factories.OpenBciWindowsObjectFactory import OpenBciWindowsObjectFactory
'''
Main starting point for Windows
'''

object_factory = OpenBciWindowsObjectFactory()
MainApp(object_factory, system_name='openbci_windows')
