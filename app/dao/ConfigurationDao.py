import json
from pathlib import Path
from shutil import copyfile

from dto.ConfigurationData import ConfigurationData


class ConfigurationDao:

    def __init__(self, system_name, recording_directory_root):
        self.default_configurations_file_path = 'data/' + system_name + '_default_configurations.json'
        self.configurations_file_path = recording_directory_root + '/configurations.json'

    def write_configuration(self, conf):
        configurations_dict = conf.get_static_attributes()
        with open(self.configurations_file_path, 'w') as configurations_file:
            configurations_file.write(json.dumps(configurations_dict, indent=4, separators=(',', ':')))
        print('Configurations have been saved')

    def read_configuration(self):
        """
        Returns CofigurationData object filled with values from configurations.json.
        Creates configurations file based on *_default_configurations.json if there is no prior configurations.json.
        """
        self._create_configurations_file_if_not_exists()

        conf = ConfigurationData()
        self.load_default_configurations(conf)
        self.load_custom_configurations(conf)
        conf.calculate_dynamic_attributes()
        return conf

    def _create_configurations_file_if_not_exists(self):
        configurations_file_path = Path(self.configurations_file_path)
        if not configurations_file_path.exists():
            copyfile(self.default_configurations_file_path, self.configurations_file_path)
            print('Created configuration file: ', configurations_file_path.absolute())

    def load_default_configurations(self, conf):
        self._load_configurations_from_file(conf, self.default_configurations_file_path)

    def load_default_configurations_without_personal_data(self, conf):
        old_conf = conf.get_copy()
        self._load_configurations_from_file(conf, self.default_configurations_file_path)
        conf.daily_sessions_counter = old_conf.daily_sessions_counter
        conf.daily_sessions_last_day = old_conf.daily_sessions_last_day
        conf.daily_sessions_program_completed = old_conf.daily_sessions_program_completed
        conf.person_name = old_conf.person_name

    def load_custom_configurations(self, conf):
        self._load_configurations_from_file(conf, self.configurations_file_path)

    def _load_configurations_from_file(self, conf, file_path):
        with open(file_path, 'r') as configurations_file:
            file_content = configurations_file.read()
            conf_json = json.loads(file_content)

            for attribute_name in conf_json.keys():
                setattr(conf, attribute_name, conf_json[attribute_name])
