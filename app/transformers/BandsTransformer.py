import numpy as np


class BandsTransformer:
    def __init__(self, conf):
        self.conf = conf

    def transform(self, epoch):
        '''
        Extracts matrix of shape( n_bands, n_channels) which contain mean values for each band fro each electrodes
        calculated based on the raw eeg epoch data
        '''
        data_length = epoch.shape[0]

        hamming_window = np.hamming(data_length)
        data_centered = epoch - np.mean(epoch, axis=0)  # Remove offset
        data_centered_hamming = (data_centered.T * hamming_window).T

        n_fft = self._next_power_of_2(data_length)
        fft_data = np.fft.fft(data_centered_hamming, n=n_fft, axis=0) / data_length
        power_spectral_densities = np.abs(fft_data[0:int(n_fft / 2), :])
        frequencies = self.conf.sampling_frequency / 2 * np.linspace(0, 1, int(n_fft / 2))

        band_powers_mean = []
        for i in range(self.conf.n_frequency_bands):
            frequency_indices, = np.where((frequencies >= self.conf.frequency_bands[i][0])
                                          & (frequencies < self.conf.frequency_bands[i][1]))
            mean = np.mean(power_spectral_densities[frequency_indices, :], axis=0)
            band_powers_mean = np.concatenate((band_powers_mean, mean), axis=0)

        return band_powers_mean.reshape(self.conf.n_frequency_bands, self.conf.n_channels)

    def _next_power_of_2(self, i):
        """
        Find the next power of 2 for number i
        """
        n = 1
        while n < i:
            n *= 2
        return n
