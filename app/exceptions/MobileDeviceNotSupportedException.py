class MobileDeviceNotSupportedException(Exception):
    '''
        Exception in case if the phone does not support Bluetooth Muse communication
    '''
    pass