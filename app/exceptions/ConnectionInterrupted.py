class ConnectionInterrupted(Exception):
    '''
        Exception in case if conncetion with headband is lost
    '''

    def __init__(self, *args: object) -> None:
        super().__init__(*args)
        self.message = "Bluetooth connection with\nthe headband lost"
