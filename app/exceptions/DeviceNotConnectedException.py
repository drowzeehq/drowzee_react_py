class DeviceNotConnectedException(Exception):
    '''
        Exception in case if the app didn't find any devices in the area
    '''
    def __init__(self, *args: object) -> None:
        super().__init__(*args)
        self.message = "Can't connect.\nMake sure the device is turned on"
