from dao.ConfigurationDao import ConfigurationDao
from dto.ApplicationContextData import ApplicationContextData
from utils.DailySessionsRestrictionHandler import DailySessionsRestrictionHandler
from transformers.BandsTransformer import BandsTransformer
from utils.SessionRecorder import SessionRecorder
from modules.DevicePlacingModule import DevicePlacingModule
from modules.BaselineModule import BaselineModule
from modules.CalibrationModule import CalibrationModule
from modules.TrainingModule import TrainingModule
from utils.communication.ReactPyTunnel import ReactPyTunnel


class MainApp():
    '''
        Class that launches the python side of the app
    '''

    def __init__(self, object_factory, system_name, recording_directory='../'):
        self.title = 'Drowzee'
        self.system_name = system_name
        self.ctx = ApplicationContextData()
        object_container = self._populate_object_container(object_factory, recording_directory)
        self.ctx.populate(object_container)
        self._populate_modules_container()
        self.on_start()

    def _populate_object_container(self, object_factory, recording_directory):
        '''
        Creation of all service objects that are later used for population of the App Context
        '''
        object_container = {}
        self._read_configurations(object_container, recording_directory)
        eeg_source = object_factory.get_EEGSource(self.conf)

        object_container['eeg_source'] = eeg_source
        object_container['noise_detector'] = object_factory.get_PhysicalNoiseDetector(self.conf, eeg_source)
        object_container['device_on_head_detector'] = object_factory.get_DeviceOnHeadDetector(self.conf)
        object_container['bands_transformer'] = BandsTransformer(self.conf)
        object_container['daily_sessions_restriction_handler'] = DailySessionsRestrictionHandler(self.ctx, self.conf)

        protocol = object_factory.get_Protocol(self.conf)
        object_container['protocol'] = protocol
        object_container['session_recorder'] = SessionRecorder(self.conf, eeg_source, protocol, self.system_name,
                                                               recording_directory)
        object_container['session_data_sender'] = object_factory.get_SessionDataSender(self.conf, recording_directory)
        return object_container

    def _populate_modules_container(self):
        '''
        Creation of all the modules that are used later for the session
        '''
        self.modules_container = {'device placing module': DevicePlacingModule(self.ctx, self.conf),
                                  'baseline module' : BaselineModule(self.ctx,self.conf),
                                  'calibration module': None,
                                  'training module': None}
        self.modules_container['calibration module'] = CalibrationModule(self.ctx, self.conf, self.modules_container)
        self.modules_container['training module'] = TrainingModule(self.ctx, self.conf)
    def _read_configurations(self, object_container, recording_directory):
        '''
            Populating app configuration from configurations.json file
        '''
        configuration_dao = ConfigurationDao(self.system_name, recording_directory)
        object_container['configuration_dao'] = configuration_dao
        self.conf = configuration_dao.read_configuration()

    def on_start(self):
        print("on start")
        self.ctx.eeg_source.connect(self._connected_callback)
        self.react_python_tunnel = ReactPyTunnel(self.conf, self.ctx, self.modules_container)


    def _connected_callback(self, error=False):
        if error:
            print("🟥",error)
        else:
            print("Device Connected !")
