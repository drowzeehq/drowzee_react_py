class ApplicationContextData:
    '''
        Contains all services and helper objects that are used across the application.
        The reason fro having the context is to be able to create this objects once
        (at the start of the app), and after to reuse the across the app, as oppose to
        creating them every time
    '''

    def populate(self, object_container):
        self.eeg_source = object_container['eeg_source']
        self.device_on_head_detector = object_container['device_on_head_detector']
        self.noise_detector = object_container['noise_detector']
        self.bands_transformer = object_container['bands_transformer']
        self.session_recorder = object_container['session_recorder']
        self.session_data_sender = object_container['session_data_sender']
        self.configuration_dao = object_container['configuration_dao']
        self.protocol = object_container['protocol']
        self.daily_sessions_restriction_handler = object_container['daily_sessions_restriction_handler']

        self.session_data = None
