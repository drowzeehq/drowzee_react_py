import copy


class ConfigurationData:
    '''
        Contains application configurations (settings) that are used across the app. The object is populated from
        json file that is located in app/data/ based on the platform that the app runs on (Android, iOS, Windows)
    '''

    """EEG sampling"""
    sampling_frequency = None
    epoch_length = None
    overlap_length = None
    eeg_source_buffer_length = None
    n_channels = None

    """Bands"""
    frequency_bands = None
    frequency_flags = None
    volume_thresholds = None
    negative_reward_thresholds = None

    """Device Placing Module"""
    quality_well_placed_threshold = None

    """Baseline Module"""
    baseline_length = None

    """Calibration Module"""
    calibration_length = None
    clean_epochs_ratio = None
    calibration_noise_buffer = None

    """Training Module"""
    training_length = None
    duration_btw_rewards = None
    duration_rewards_while_one = None

    """Eyes opened or closed detector"""
    eye_detector_channel_indices = None
    eye_detector_activity_threshold = None
    eye_detector_buffer_size = None

    """Physical noise detector"""
    positive_electrode_thresholds = None
    negative_electrode_thresholds = None

    """Audio smoothing"""
    smoothing_transformer = None
    vector_length_transformer_buffer_size = None

    """Sounds Box"""
    sounds_path = None

    """User data"""
    person_name = None

    dynamic_attributes = ['shift_length', 'n_frequency_bands']

    def calculate_dynamic_attributes(self):
        """
        Calculates dynamic attributes, values that are not read from file.
        Should be called each time after normal attributes are updated
        to perform recalculation based on updated values
        """
        self.shift_length = round(self.epoch_length - self.overlap_length, 3)

        self.n_frequency_bands = len(self.frequency_bands)


    def get_all_attributes(self):
        return copy.deepcopy(self.__dict__)

    def get_static_attributes(self):
        attributes = copy.deepcopy(self.__dict__)
        for attribute_name in self.dynamic_attributes:
            attributes.pop(attribute_name, None)
        attributes.pop('dynamic_attributes', None)
        return attributes

    def get_copy(self):
        return copy.deepcopy(self)
