class EpochData:
    '''
        Contains calculated metrics for each epoch
    '''
    epoch_id = None
    is_signal_noisy = None
    is_eyes_opened = None
    n_epochs_collected = None

    def __init__(self, n_epochs_total):
        self.n_epochs_total = n_epochs_total


class CalibrationEpochData(EpochData):
    clean_epochs_ratio = None

    def __init__(self, n_epochs_total):
        super().__init__(n_epochs_total)


class TrainingEpochData(EpochData):
    is_neurofeedback = None
    n_current_coins = 0

    def __init__(self, n_epochs_total):
        super().__init__(n_epochs_total)
