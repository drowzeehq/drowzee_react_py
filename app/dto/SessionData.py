from time import strftime, localtime


class SessionData:
    '''
        Each session has certain values to store
    '''

    # Raw data from device placing module, saved to *_raw_placing.csv
    device_placing_buffer_raw = []

    # Metrics data from device placing module, saved to *_metrics_placing.csv
    device_placing_buffer_metrics = []

    # Raw data from baseline module, saved to *_raw_calibration.csv
    baseline_buffer_raw = []

    # Metrics data from baseline module, saved to *_metrics_calibration.csv
    baseline_buffer_metrics = []

    # Raw data from calibration module, saved to *_raw_calibration.csv
    calibration_buffer_raw = []

    # Metrics data from calibration module, saved to *_metrics_calibration.csv
    calibration_buffer_metrics = []

    # Raw data from calibration module, saved to *_raw_training.csv
    training_buffer_raw = []
    # Metrics data from training module, saved to *_metrics_training.csv
    training_buffer_metrics = []

    # Mean powers for each electrode and each band computed during calibration
    calibration_mean_powers = None
    # Smily face score at the end of the training session, "-1" is the default value, which means "not provided"
    feedback_score = -1
    # Textfeedback
    feedback_text = None
    # Contains last raw EEG epoch
    last_epoch = None
    last_timestamp = None
    # Initial value for training performance score
    training_score = 0
    # Initial value for signal quality score
    noise_score = 0

    def __init__(self, app_configurations):
        self.person_name = app_configurations.person_name
        # timestamp of the session beginning (takes current time)
        self.timestamp = strftime("%Y-%m-%d-%H.%M.%S", localtime())
        self.app_configurations = app_configurations
