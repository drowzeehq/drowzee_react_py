from utils.send_data.AbstractSessionDataSender import AbstractSessionDataSender


class WindowsSessionDataSender(AbstractSessionDataSender):
    def __init__(self, conf, recording_directory_root):
        super().__init__(conf, recording_directory_root)

    def check_network_enabled(self):
        return True
