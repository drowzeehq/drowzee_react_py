import shutil
import zipfile
from bottle import *
from threading import Thread
import logging

class AbstractSessionDataSender:
    '''
        Parent class for sending session data to server. Subclassed implement OS platform specific logic
    '''

    def __init__(self, conf, recording_directory_root):
        self.conf = conf
        self.session_recordings_directory = recording_directory_root + '/session_recordings/'
        self.thread_sending_zip = Thread(target=self.server_creation, args=(self,))
        self.thread_sending_zip.start()

    def server_creation(self, *args):
        self.bottle = Bottle()
        self.bottle.route('/download/<session_uuid>', method='GET', callback=self.download)
        self.bottle.run(host='10.0.0.14',port=3030)
        logging.info('Server 10.0.0.14 listening on port 3030...')

    def is_data_folder_empty(self):
        '''
        Check if data folder is empty
        '''
        for root, directories, files in os.walk(self.data_folder_path):
            if directories:
                return False
            elif files:
                return False
        return True

    def check_network_enabled(self):
        '''
        Implemtation is in child classes
        :return:
        '''
        raise NotImplementedError

    def delete_data(self):
        '''
        Close connection with the server
        '''
        if self.is_data_folder_empty() == False:
            shutil.rmtree(self.data_folder_path)
            os.unlink(self.zip_file_name)
            print('Files removed')

    def _zip_session_files(self):
        '''
        Zips content of the session_recordings folder
        '''
        data_file_paths = self._retrieve_data_file_paths()
        zip_file_path = self.session_recordings_directory + self.zip_file_name

        zip_file = zipfile.ZipFile(zip_file_path, 'w')
        with zip_file:
            for file in data_file_paths:
                zip_file.write(file, file[file.find('session_recordings'):])

    def _retrieve_data_file_paths(self):
        file_paths = []
        for root, directories, files in os.walk(self.data_folder_path):
            for filename in files:
                file_path = os.path.join(root, filename)
                file_paths.append(file_path)
        return file_paths

    def download(self,session_uuid):
        self.session_uuid = session_uuid
        self.data_folder_path = self.session_recordings_directory + 'session_'+ self.session_uuid
        self.zip_file_name = self.session_uuid + '.zip'

        if self.is_data_folder_empty():
            return "No Files"
        else :
            self._zip_session_files()
            root_path = self.session_recordings_directory
            return static_file(self.zip_file_name, root=root_path, download=True)