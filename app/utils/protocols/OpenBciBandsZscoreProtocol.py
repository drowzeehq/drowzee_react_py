import numpy as np
import pandas as pd
import random
from collections import deque
import logging

class OpenBciBandsZScoreProtocol:

    def __init__(self, conf):
        '''
        Class constructor for the protocol, it includes the following parameters:
            conf: configuration
            threshold_names: Name for the thresholds
            means_tables: Retrieves mean values for each location and frequency
            sd_table: Retrieves standard deviation values for each location and frequency
            zscores_table: An empty table that will contain the zscore value for each frequency and location
            thresholds_table = An empty table that will contain the thresholds for each frequency and location
            zvalue: Value that all z-scores should aim to be at
        '''
        self.conf = conf
        self.threshold_names = ['OpenBCI Bands ZScore']
        self.global_means_table = pd.read_csv("data/zscore_csv/means.csv")
        self.global_sd_table = pd.read_csv("data/zscore_csv/SDs.csv")

        self.frequency_bands = self.conf.frequency_bands
        self.locations = ["C3", "C4", "C3", "C4"]
        '''
        
        Set up the table of values for the frequency bands.
        '''
        self.bands_means_table = self.construct_table(0, self.conf.n_channels, self.conf.n_frequency_bands)
        self.bands_sd_table = self.construct_table(0, self.conf.n_channels, self.conf.n_frequency_bands)
        self.calculate_bands_tables()

        self.zscores_table = self.construct_table(0, self.conf.n_channels, self.conf.n_frequency_bands)
        self.thresholds_table = self.construct_table(0, self.conf.n_channels, self.conf.n_frequency_bands)
        self.zvalue = 1 # For now lets set the Z score threshold to +/- 1 std

        '''
                Sets up a table containing the flags for every location and frequency if they have to be trained or not. 
                Default set to False.'''
        self.rewarding = self.construct_table(False, self.conf.n_channels, self.conf.n_frequency_bands)


        '''
        Sets up values for the update of the thresholds
        '''
        self.update_percentage_time = int(60 * (self.conf.epoch_length / self.conf.shift_length))
        self.number_rewards = [[deque(maxlen=self.update_percentage_time) for i in range(self.conf.n_channels)] for j in range(self.conf.n_frequency_bands)] # rewards from the last minute
        self.reward_rate_duration = int(10 * (self.conf.epoch_length / self.conf.shift_length))
        self.number_epoch = 0
        self.should_play_feedback_deque = deque(maxlen=self.reward_rate_duration)
        self.feedback_rate = 0.7

        self.percentage = self.construct_table(0.1, self.conf.n_channels, self.conf.n_frequency_bands)
        self.reward_rate_table = self.construct_table(0, self.conf.n_channels, self.conf.n_frequency_bands)
        self.set_thresholds()


    def perform_calibration(self,ctx,calibration_epochs, n_calibration_epochs):
        self.mean_powers_per_channel_calib = self._calculate_mean_powers_per_channel(ctx, calibration_epochs,
                                                                                     n_calibration_epochs)
        self.calibration_thresholds = self.calculate_calibration_thresholds(self.mean_powers_per_channel_calib)
        self.training_thresholds = self.calibration_thresholds

    def perform_training(self, ctx, is_signal_noisy, epoch, protocol_scores_array):
        self.protocol_scores_array = protocol_scores_array
        bands_power_mean = ctx.bands_transformer.transform(epoch)
        self.protocol_scores = self.get_scores(bands_power_mean)
        self.protocol_scores_mean = np.mean(self.protocol_scores)

        self.calculate_zscores(bands_power_mean)
        are_thresholds_reached = self.set_rewarding()
        logging.debug("rewarding : {}".format(self.rewarding))
        print(self.rewarding)
        self.update_reward_rate()
        logging.debug("reward rate table : {}".format(self.reward_rate_table))
        print(self.reward_rate_table)

        self.should_play_feedback = are_thresholds_reached and not is_signal_noisy
        self.should_play_feedback_deque.append(self.should_play_feedback)

        if (self.should_play_feedback and sum(self.should_play_feedback_deque) > (self.feedback_rate*self.update_percentage_time)):
            logging.info("Update thresholds ! old threshold : {}".format(self.percentage))
            self.update_percentage()
            self.should_play_feedback_deque.clear()
            logging.info("New thresholds: {}".format(self.percentage))

        self.training_thresholds = self.percentage

    def _calculate_mean_powers_per_channel(self, ctx, calibration_epochs, n_calibration_epochs):
        mean_powers_per_channel = np.zeros((n_calibration_epochs, self.conf.n_frequency_bands, self.conf.n_channels))
        for i in range(n_calibration_epochs):
            mean_powers_per_channel[i] = ctx.bands_transformer.transform(calibration_epochs[i])
        return mean_powers_per_channel

    def get_scores(self, bands_power_mean):
        '''
        Retrieves the values for each frequency and location
        Arguments:
            bands_power_mean: Mean of the measured value at each frequency and location in one epoch
        Returns:
            The mean value for that frequency and location? This is redundant and needs to be reviewed
        '''
        return [bands_power_mean[3][j] for j in range(4)]

    def calculate_zscores(self, bands_power_mean):
        '''Constructs the zscore table for the latest epoch.
        Parameters:
            bands_power_mean: Mean of measured values at each frequency and location in the epoch
        Returns:
            zscores_table: Table with zscores
        '''
        for frequency_band in range(self.conf.n_frequency_bands):
            for location_index in range(self.conf.n_channels):
                self.zscores_table[frequency_band][location_index] = (bands_power_mean[frequency_band][location_index]
                                                                   - self.bands_means_table[frequency_band][location_index])\
                                                                  / self.bands_sd_table[frequency_band][location_index]
        #print(self.zscores_table)

    def set_thresholds(self):
        '''
        Sets up the threshold value for each location. Returns such table.
        '''

        for frequency_band in range(self.conf.n_frequency_bands):
            for location_index in range(self.conf.n_channels):
                self.thresholds_table[frequency_band][location_index] = self.zvalue * self.bands_sd_table[frequency_band][location_index] + \
                                                             self.bands_means_table[frequency_band][location_index]

    def set_rewarding(self):
        '''
        Defines if a reward has to be administered or not. This is done by checking if the absolute value of the zscore is smaller than the absolute value of the threshold.
        Arguments:
            percentage: Table with the percentage of values that have to be above the threshold.
        '''
        print(self.zscores_table)
        print(self.thresholds_table)
        for frequency_band in range(self.conf.n_frequency_bands):
            for location_index in range(self.conf.n_channels):
                print(self.number_rewards[frequency_band][location_index])
                if abs(self.zscores_table[frequency_band][location_index]) < abs(self.thresholds_table[frequency_band][location_index]):
                    self.number_rewards[frequency_band][location_index].append(1)
                    if self.reward_rate_table[frequency_band][location_index] > self.percentage[frequency_band][location_index]:
                        self.rewarding[frequency_band][location_index] = True
                else:
                    self.number_rewards[frequency_band][location_index].append(0)
                    self.rewarding[frequency_band][location_index] = False
        reward_per_band = []
        for frequency_band in range(self.conf.n_frequency_bands):
            if self.rewarding[frequency_band] == [True, True, True, True]:
                reward_per_band.append(True)
        return (sum(reward_per_band) >= 3)

        #true_list = [True for i in range(self.conf.n_channels)]
        #if all(rewarding == true_list for rewarding in self.rewarding):
        #    return True
        #else:
        #    return False


    def construct_table(self, value, n_channels, n_frequency_bands):
        '''
        Sets up an empty table that will return the rate of epochs under the threshold. 
        Default set to 0.
        Arguments:
            frequencies: Frequencies being measured
            locations: Locations being used 
        Returns such table
        '''
        reward_table = [[value for l in range(n_channels)] for f in range(n_frequency_bands)]
        return reward_table

    def update_reward_rate(self):
        '''
        Checks the percentage of epochs that have been within the acceptable threshold during a set period of time.
        '''
        for frequency_band in range(self.conf.n_frequency_bands):
            for location in range(self.conf.n_channels):
                number_rewards = sum(self.number_rewards[frequency_band][location])
                print(number_rewards)
                self.reward_rate_table[frequency_band][location] = \
                    number_rewards / self.update_percentage_time

    def update_percentage(self):
        '''
        Updates the percentage of values that have to be within a zvalue after a set period of time (eg, one minute). 
        If the rate of rewards is greater than such percentage and the percentage is less than 95 percent, we will update that percentage to +0.01.  
        '''
        for frequency_band in range(self.conf.n_frequency_bands):
            for location in range(self.conf.n_channels):
                if self.reward_rate_table[frequency_band][location] > self.percentage[frequency_band][location] \
                        and self.percentage[frequency_band][location] <= 0.9:
                    self.percentage[frequency_band][location] += 0.1

    def calculate_bands_tables(self):

        for frequency_band in range(self.conf.n_frequency_bands):
            for location in self.locations:
                for location_index in range(self.conf.n_channels):
                    means_per_band_per_loc = []
                    stds_per_band_per_loc = []
                    for frequency in self.global_means_table.index:
                        if self.frequency_bands[frequency_band][0] <= frequency < self.frequency_bands[frequency_band][1]:
                            means_per_band_per_loc.append(self.global_means_table.at[frequency, location])
                            stds_per_band_per_loc.append(self.global_sd_table.at[frequency, location])
                    self.bands_means_table[frequency_band][location_index] = np.mean(means_per_band_per_loc)
                    self.bands_sd_table[frequency_band][location_index] = np.mean(stds_per_band_per_loc)
        print(self.bands_means_table)
        print(self.bands_sd_table)

    # Everything here below is code from other protocols that idk if it is necessary or not.
    # No, it can be removed, but the code in the modules needs to be adjusted to support new functions

    def get_threshold_names(self):
        return self.threshold_names

    def calculate_mean_threshold(self, epoch_metrics):
        return float(epoch_metrics[4])

    def calculate_calibration_thresholds(self, mean_powers_per_channel):
        protocol_scores_array = np.asarray([mean_powers_per_channel[:, 3, j] for j in range(4)]).T # 3 as a fixed value since it is just beta range
        return self.calculate_training_thresholds(protocol_scores_array)

    def calculate_training_thresholds(self, protocol_scores_array):
        quantile = 1 - self.conf.training_rewarding
        return [np.quantile(protocol_scores_array[:, i], q=quantile, axis=0) for i in range(4)] # Range is flat 4 for the same r