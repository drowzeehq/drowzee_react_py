import numpy as np
import pandas as pd
import random

class MuseZScoreProtocol:

    def __init__(self, conf, n_frequencies, n_locations):
        '''
        Class constructor for the protocol, it includes the following parameters:
            conf: configuration
            threshold_names: Name for the thresholds
            means_tables: Retrieves mean values for each location and frequency
            sd_table: Retrieves standard deviation values for each location and frequency
            zscores_table: An empty table that will contain the zscore value for each frequency and location
            thresholds_table = An empty table that will contain the thresholds for each frequency and location
            zvalue: Value that all z-scores should aim to be at
        '''
        self.conf = conf
        self.threshold_names = ['Muse ZScore']
        self.means_table = pd.read_csv("../../data/zscore_csv/means.csv")
        self.sd_table = pd.read_csv("../../data/zscore_csv/SDs.csv")
        self.zscores_table = pd.DataFrame(columns= self.means_table.columns, index = self.means_table.index)
        self.thresholds_table = pd.DataFrame(columns= self.means_table.columns, index = self.means_table.index)
        self.zvalue = 1 #For now lets set the Z score threshold to +/- 1

        '''
                Sets up a table containing the flags for every location and frequency if they have to be trained or not. 
                Default set to False.'''
        self.rewarding = [[False for f in range(n_frequencies)] for l in range(n_locations)]
        
       
    def get_scores(self, bands_power_mean):
        '''
        Retrieves the values for each frequency and location
        Arguments:
            bands_power_mean: Mean of the measured value at each frequency and location in one epoch
        Returns:
            The mean value for that frequency and location? This is redundant and needs to be reviewed
        '''
        return [bands_power_mean[3][j] for j in range(4)]

    def calculate_zscores(self, bands_power_mean):
        '''Constructs the zscore table for the latest epoch.
        Parameters:
            bands_power_mean: Mean of measured values at each frequency and location in the epoch
        Returns:
            zscores_table: Table with zscores
        '''
        for location in self.zscores_table.columns:
            for frequency in range(len(self.zscores_table.index)):
                self.zscores_table[location][frequency] = (bands_power_mean[frequency][location] - self.means_table[frequency, location]) /self.sd_table[frequency, location]
                
        return self.zscores_table

    def set_thresholds(self):
        '''
        Sets up the threshold value for each location. Returns such table.
        '''
        for location in self.zscores_table.columns:
            for frequency in range(len(self.zscores_table.index)):
                self.thresholds_table[location][frequency] = self.zvalue * self.sd_table[frequency,location] + self.means_table[frequency,location]
        return self.thresholds_table

    def set_rewarding(self, percentage):
        '''
        Defines if a reward has to be administered or not. This is done by checking if the absolute value of the zscore is smaller than the absolute value of the threshold.
        Arguments:
            percentage: Table with the percentage of values that have to be above the threshold.
        '''
        for location in self.zscores_table.columns:
            for frequency in range(len(self.zscores_table.index)):
                if random.uniform(0,1) > percentage[frequency][location]:
                    if abs(self.zscores_table[frequency][location]) < abs(self.thresholds_table[frequency][location]):
                        self.rewarding[frequency][location] = True
                    
                else:
                    self.rewarding[frequency][location] = False


    def construct_reward_rate_table(self, locations, frequencies):
        '''
        Sets up an empty table that will return the rate of epochs under the threshold. 
        Default set to 0.
        Arguments:
            frequencies: Frequencies being measured
            locations: Locations being used 
        Returns such table
        '''
        rewards_rate = [[0 for f in frequencies] for l in locations]
        return rewards_rate
    
    def set_reward_rate(self, reward_rate, epoch_means):
        '''
        Checks the percentage of epochs that have been within the acceptable threshold. 
        Arguments:
            epoch_means: Contains a list of mean values for every location and frequency for every epoch. 
        '''
        
        return reward_rate
   
    def update_percentage(self, percentage, reward_rate):
        '''
        Updates the percentage of values that have to be within a zvalue after a set period of time (eg, one minute). 
        If the rate of rewards is greater than such percentage and the percentage is less than 95 percent, we will update that percentage to +0.01.  
        Arguments:
            percentage: Table with the percentage of values that have to be above the threshold
            reward_rate: The percentage of epochs that have have been measured under the threshold. 
        Returns:
            percentage: Updated values for the percentages
        '''
        for location in self.zscores_table.columns:
            for frequency in range(len(self.zscores_table.index)):
                if self.rewarding[frequency][location] == True:
                    if reward_rate[frequency][location] > percentage[frequency][location] and percentage[frequency][location] <= 0.95:
                        percentage[frequency][location] += 0.01

        return percentage

    #Everything here below is code from other protocols that idk if it is necessary or not.
    #No, it can be removed, but the code in the modules needs to be adjusted to support new functions

    def get_threshold_names(self):
        return self.threshold_names 

    def calculate_calibration_thresholds(self, mean_powers_per_channel):
        protocol_scores_array = np.asarray([mean_powers_per_channel[:, 3, j] for j in range(4)]).T # 3 as a fixed value since it is just beta range
        return self.calculate_training_thresholds(protocol_scores_array)

    def calculate_training_thresholds(self, protocol_scores_array):
        quantile = 1 - self.conf.training_rewarding
        return [np.quantile(protocol_scores_array[:, i], q=quantile, axis=0) for i in range(4)] # Range is flat 4 for the same reason: one band for 4 electrodes


    # Rewarding if the threshold is above the scores (ie: The scores are below the threshold) at all locations
    def is_threshold_reached(self, protocol_scores, thresholds):
        return (protocol_scores[0] < thresholds[0] and protocol_scores[1] < thresholds[1] and
                protocol_scores[2] < thresholds[2] and protocol_scores[3] < thresholds[3])
    '''
    def update_thresholds(self, current_thresholds, new_thresholds):
        # We update the threshold if the new threshold is lower than the current one
        # allow_threshold_decrease is just a name, here it would actually mean if it increases
        for i in range(len(current_thresholds)):
            if new_thresholds[i] < current_thresholds[i]:
                current_thresholds[i] = new_thresholds[i]
                self.allow_threshold_increase[i] = True
            elif new_thresholds[i] > current_thresholds[i]:
                if self.allow_threshold_increase[i]:
                    current_thresholds[i] = new_thresholds[i]
                self.allow_threshold_increase[i] = False
            else:
                self.allow_threshold_increase[i] = False
    '''
    def calculate_mean_threshold(self, epoch_metrics):
        return float(epoch_metrics[4])
