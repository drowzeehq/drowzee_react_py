import numpy as np


class BrainBitThetaProtocol:

    def __init__(self, conf):
        self.conf = conf
        self.threshold_names = ['Mean_Delta_Theta']
        self.allow_threshold_decrease = [True, True, True, True, True, True, True, True]

    def get_threshold_names(self):
        return self.threshold_names

    def calculate_calibration_thresholds(self, mean_powers_per_channel):
        protocol_scores_array = np.asarray([mean_powers_per_channel[:, i, j] for i in range(0, 2) for j in range(4)]).T
        return self.calculate_training_thresholds(protocol_scores_array)

    def calculate_training_thresholds(self, protocol_scores_array):
        quantile = 1 - self.conf.training_rewarding
        return [np.quantile(protocol_scores_array[:, i], q=quantile, axis=0) for i in range(8)]

    def get_scores(self, bands_power_mean):
        return [bands_power_mean[i][j] for i in range(0, 2) for j in range(4)]

    def is_threshold_reached(self, protocol_scores, thresholds):
        return (protocol_scores[0] > thresholds[0] and protocol_scores[1] > thresholds[1] and \
                protocol_scores[2] > thresholds[2] and protocol_scores[3] > thresholds[3]) or \
               (protocol_scores[4] > thresholds[4] and protocol_scores[5] > thresholds[5] and \
                protocol_scores[6] > thresholds[6] and protocol_scores[7] > thresholds[7])

    def update_thresholds(self, current_thresholds, new_thresholds):
        for i in range(len(current_thresholds)):
            if new_thresholds[i] > current_thresholds[i]:
                current_thresholds[i] = new_thresholds[i]
                self.allow_threshold_decrease[i] = True
            elif new_thresholds[i] < current_thresholds[i]:
                if self.allow_threshold_decrease[i]:
                    current_thresholds[i] = new_thresholds[i]
                self.allow_threshold_decrease[i] = False
            else:
                self.allow_threshold_decrease[i] = False

    def calculate_mean_threshold(self, epoch_metrics):
        return float(epoch_metrics[4])
