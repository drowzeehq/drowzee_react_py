import numpy as np


class MuseBetaProtocol:

    def __init__(self, conf):
        self.conf = conf
        self.threshold_names = ['Mean_Beta']
        self.allow_threshold_increase = [True, True, True, True]  # 4 Since it is only one band and 4 electrodes
        self.should_play_feedback = False
        self.protocol_scores_array_index = 0
        self.protocol_scores_array_size = int(90 * (self.conf.epoch_length / self.conf.shift_length))
        self.training_thresholds = None
        self.protocol_scores_array = None
        self.protocol_scores = None
        self.protocol_scores_mean = None

    def perform_calibration(self, ctx, calibration_epochs, n_calibration_epochs ):
        self.mean_powers_per_channel_calib = self._calculate_mean_powers_per_channel(ctx, calibration_epochs,
                                                                          n_calibration_epochs)
        self.calibration_thresholds = self.calculate_calibration_thresholds(self.mean_powers_per_channel_calib)
        self.training_thresholds = self.calibration_thresholds
        
    def perform_training(self, ctx, is_signal_noisy, epoch, protocol_scores_array):
        self.protocol_scores_array = protocol_scores_array
        bands_power_mean = ctx.bands_transformer.transform(epoch)
        self.protocol_scores = self.get_scores(bands_power_mean)
        self.protocol_scores_mean = np.mean(self.protocol_scores)

        is_theta_threshold_reached = self.is_threshold_reached(self.protocol_scores, self.training_thresholds)
        self.should_play_feedback = is_theta_threshold_reached and not is_signal_noisy
        if not is_signal_noisy:
            self._update_protocol_thresholds(self.protocol_scores)

    def get_threshold_names(self):
        return self.threshold_names

    def _calculate_mean_powers_per_channel(self, ctx, calibration_epochs, n_calibration_epochs):
        mean_powers_per_channel = np.zeros((n_calibration_epochs, self.conf.n_frequency_bands, self.conf.n_channels))
        for i in range(n_calibration_epochs):
            mean_powers_per_channel[i] = ctx.bands_transformer.transform(calibration_epochs[i])
        return mean_powers_per_channel

    def calculate_calibration_thresholds(self, mean_powers_per_channel):
        protocol_scores_array = np.asarray([mean_powers_per_channel[:, 3, j] for j in range(4)]).T # 3 as a fixed value since it is just beta range
        return self.calculate_training_thresholds(protocol_scores_array)

    def calculate_training_thresholds(self, protocol_scores_array):
        quantile = 1 - self.conf.training_rewarding
        return [np.quantile(protocol_scores_array[:, i], q=quantile, axis=0) for i in range(4)] # Range is flat 4 for the same reason: one band for 4 electrodes

    def get_scores(self, bands_power_mean):
        return [bands_power_mean[3][j] for j in range(4)]

    # Rewarding if the threshold is above the scores (ie: The scores are below the threshold) at all locations
    def is_threshold_reached(self, protocol_scores, thresholds):
        return (protocol_scores[0] < thresholds[0] and protocol_scores[1] < thresholds[1] and
                protocol_scores[2] < thresholds[2] and protocol_scores[3] < thresholds[3])

    def _update_protocol_thresholds(self, protocol_scores):
        '''
        Adds protocol values for new epoch and each 90 epochs recalculates the training threshold values
        '''
        self.protocol_scores_array[self.protocol_scores_array_index, :] = protocol_scores
        self.protocol_scores_array_index += 1
        if self.protocol_scores_array_index >= self.protocol_scores_array_size:
            self.protocol_scores_array_index = 0
            new_training_thresholds = self.calculate_training_thresholds(self.protocol_scores_array)
            print(new_training_thresholds)
            self.update_thresholds(new_training_thresholds)

    def update_thresholds(self, new_thresholds):
        # We update the threshold if the new threshold is lower than the current one
        # allow_threshold_decrease is just a name, here it would actually mean if it increases
        for i in range(len(self.training_thresholds)):
            if new_thresholds[i] < self.training_thresholds[i]:
                self.training_thresholds[i] = new_thresholds[i]
                self.allow_threshold_increase[i] = True
            elif new_thresholds[i] > self.training_thresholds[i]:
                if self.allow_threshold_increase[i]:
                    self.training_thresholds[i] = new_thresholds[i]
                self.allow_threshold_increase[i] = False
            else:
                self.allow_threshold_increase[i] = False

    def calculate_mean_threshold(self, epoch_metrics):
        return float(epoch_metrics[4])
