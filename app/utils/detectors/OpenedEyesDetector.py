from collections import deque

import numpy as np


class OpenedEyesDetector:

    def __init__(self, conf):
        self.conf = conf
        self.electrode_indices = conf.eye_detector_channel_indices
        self.n_channels = len(conf.eye_detector_channel_indices)
        self.theta_powers = []
        self.alpha_powers = []
        self.beta_powers = []
        for _ in range(self.n_channels):
            self.theta_powers.append(deque(maxlen=conf.eye_detector_buffer_size))
            self.alpha_powers.append(deque(maxlen=conf.eye_detector_buffer_size))
            self.beta_powers.append(deque(maxlen=conf.eye_detector_buffer_size))

    def detect(self, epoch):
        """
        Detection of weather eyes are opened or closed during the given epoch, based on alpha band power
        :param epoch:
        :return 1 if alpha/beta is less then activity_threshold, 0 otherwise:
        """
        theta_powers_sum = 0
        alpha_powers_sum = 0
        beta_powers_sum = 0
        for i in range(self.n_channels):
            signal_amplitude_power = abs(np.fft.fft(epoch[:, self.electrode_indices[i]]))
            theta_powers_mean = np.mean(signal_amplitude_power[6:8])
            alpha_powers_mean = np.mean(signal_amplitude_power[8:12])
            beta_powers_mean = np.mean(signal_amplitude_power[12:14])
            self.theta_powers.append(theta_powers_mean)
            self.alpha_powers.append(alpha_powers_mean)
            self.beta_powers.append(beta_powers_mean)
            theta_powers_sum += theta_powers_mean
            alpha_powers_sum += alpha_powers_mean
            beta_powers_sum += beta_powers_mean
        alpha_beta_relation = alpha_powers_sum / ((beta_powers_sum + theta_powers_sum) / 2)
        return int(alpha_beta_relation < self.conf.eye_detector_activity_threshold)
