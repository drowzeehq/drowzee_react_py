import numpy as np


class MuseDeviceOnHeadDetector:
    '''
    Detects whether the headband is on head
    '''

    def __init__(self, conf):
        self.conf = conf
        # For device to be on head minimum 2 channels should be intact
        self.n_minimum_intact_channels = 2

        # A queue of last 8 epochs that contain binary information whether the devices was on head or wasn't
        self.device_on_head_buffer = np.zeros((8, conf.n_channels))
        self.buffer_empty_space = 0

    def detect(self, epoch):
        '''
        Returns 1 if device is on head, 0 if not
        '''
        return int(sum(self.detect_per_channel(epoch) > 0) >= self.n_minimum_intact_channels)

    def detect_per_channel(self, epoch):
        '''
        Returns [1, 1, 1, 1] (for each channel) if device is on head, otherwise returns another values
        '''
        epoch_means = np.mean(epoch, axis=0)
        self._update_buffer(epoch_means)
        return np.mean(self.device_on_head_buffer[-self.buffer_empty_space:], axis=0)

    def _update_buffer(self, epoch_means):
        '''
        Updates self.device_on_head_buffer based on new epoch's epoch_means
        '''
        on_head_flags = np.logical_and(epoch_means > self.conf.on_head_min_electrode_threshold,
                                       epoch_means < self.conf.on_head_max_electrode_threshold)
        self.device_on_head_buffer[:-1, :] = self.device_on_head_buffer[1:, :]
        self.device_on_head_buffer[-1, :] = on_head_flags
        self.buffer_empty_space += 0 if self.buffer_empty_space == len(self.device_on_head_buffer) else 1
