from collections import deque

import numpy as np


class MusePhysicalNoiseDetector:
    '''
    Detects whether there are any EEG artifacts. Based on local std window of 25 epochs
    '''

    def __init__(self, conf, eeg_source):
        self.conf = conf
        self.shift_n_samples = eeg_source.shift_n_samples

        # Buffer that contains (mean + self.conf.noise_deviation_threshold * std) for the last 25 epochs
        self.pos_deviations = deque(maxlen=25)

        # Buffer that contains (mean - self.conf.noise_deviation_threshold * std) for the last 25 epochs
        self.neg_deviations = deque(maxlen=25)
        self.clean_signal = np.zeros(conf.n_channels)

    def detect(self, epoch):
        """
            Returns 0 if there is no noise in all electrodes,
            returns 1 if there is  a noise at least in one electrode
        """
        noise_flags = self.detect_per_electrode(epoch)
        return int(sum(noise_flags) != 0)

    def detect_per_electrode(self, epoch):
        """
            Returns [0, 0, 0, 0] if there is no noise in all electrodes,
            returns other array values otherwise
        """

        epoch_chunk = self._remove_DC_offset(epoch[-self.shift_n_samples:, :])
        epoch_max = np.max(epoch_chunk, axis=0)
        epoch_min = np.min(epoch_chunk, axis=0)
        neg_deviation, pos_deviation, epoch_std = self._calculate_deviations(epoch_chunk)

        noise_flags = self._is_signal_noisy(epoch_min, epoch_max, neg_deviation, pos_deviation)

        self.pos_deviations.append(pos_deviation)
        self.neg_deviations.append(neg_deviation)
        return noise_flags

    def _calculate_deviations(self, data):
        mean = np.mean(data, axis=0)
        std = np.std(data, axis=0)
        pos_deviation = mean + self.conf.noise_deviation_threshold * std
        neg_deviation = mean - self.conf.noise_deviation_threshold * std
        return neg_deviation, pos_deviation, std

    def _is_threshold_exceeded(self, data_min, data_max, min_threshold, max_threshold):
        return (data_min < min_threshold) | (data_max > max_threshold)

    def _is_signal_noisy(self, epoch_min, epoch_max, neg_deviation, pos_deviation):
        '''
        If the maximum value of epoch is below pos_deviation and if minimum epoch value is above neg_deviation
        then there is no noise. Otherwise there is noise
        '''
        if len(self.pos_deviations) == 0:
            return self._is_threshold_exceeded(epoch_min, epoch_max, neg_deviation, pos_deviation)

        mean_pos_deviation = np.mean(self.pos_deviations, axis=0)
        mean_neg_deviation = np.mean(self.neg_deviations, axis=0)
        return self._is_threshold_exceeded(epoch_min, epoch_max, mean_neg_deviation, mean_pos_deviation)

    def _remove_DC_offset(self, epoch_chunk):
        return epoch_chunk - np.mean(epoch_chunk, axis=0)

    def clear_buffer(self):
        self.pos_deviations = deque(maxlen=25)
        self.neg_deviations = deque(maxlen=25)
