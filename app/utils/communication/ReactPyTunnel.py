import websockets
import asyncio
import json
from dto.SessionData import SessionData
from utils.configurationModifier.ConfigurationModifier import ConfigurationModifier

import threading
import logging 

class ReactPyTunnel:
    '''
        Class that create the tunnel between React and Python and process the communication
    '''
    def __init__(self, conf, ctx, modules):
        self.conf = conf
        self.ctx = ctx
        self.socket = None
        self.device_placing_module = modules['device placing module']
        self.baseline_module = modules['baseline module']
        self.calibration_module = modules['calibration module']
        self.training_module = modules['training module']
        self.protocol = ctx.protocol
        self.baseline_thread = None
        self.device_placing_thread = None
        self.calibration_thread = None
        self.training_thread = None
        self.configuration_modifier = ConfigurationModifier(self.ctx, self.conf)
        self.recording_dir = None
        self.create_websocket_server()
        self.Logger = None


    def create_websocket_server(self, port=2020):
        self.websocket_server = websockets.serve(self.on_connection, '10.0.0.14', port)
        asyncio.get_event_loop().run_until_complete(self.websocket_server)
        asyncio.get_event_loop().run_forever()

    async def on_connection(self, websocket, path):
        while True:
            logging.info('Waiting for an event')
            self.websocket = websocket

            data = await self.websocket.recv()
            event = eval(data)
            if (event['name'] == "set_session_id"):
                self.session_id = str(event['session_id'])
                self.start_session()

            elif (event['name'] == "current_settings"):
                logging.info('current settings requested')
                settings = {'on_head_min_electrode_threshold': self.conf.on_head_min_electrode_threshold,
                                    'on_head_max_electrode_threshold': self.conf.on_head_max_electrode_threshold,
                                    'negative_electrode_threshold': self.conf.negative_electrode_threshold,
                                    'positive_electrode_threshold': self.conf.positive_electrode_threshold,
                                    'noise_deviation_threshold': self.conf.noise_deviation_threshold,
                                    'training_module_noise_sound_enabled': self.conf.training_module_noise_sound_enabled,
                                    'training_rewarding': self.conf.training_rewarding,
                                    'training_length': self.conf.training_length
                                    }
                event = json.dumps({'name': 'settings',
                                    'settings': settings
                                    })
                await self.websocket.send(event)

            elif (event['name'] == "default_settings"):
                logging.info('default settings requested')
                self.ctx.configuration_dao.load_default_configurations_without_personal_data(self.conf)
                self.conf.calculate_dynamic_attributes()
                settings = {'on_head_min_electrode_threshold': self.conf.on_head_min_electrode_threshold,
                                    'on_head_max_electrode_threshold': self.conf.on_head_max_electrode_threshold,
                                    'negative_electrode_threshold': self.conf.negative_electrode_threshold,
                                    'positive_electrode_threshold': self.conf.positive_electrode_threshold,
                                    'noise_deviation_threshold': self.conf.noise_deviation_threshold,
                                    'training_module_noise_sound_enabled': self.conf.training_module_noise_sound_enabled,
                                    'training_rewarding': self.conf.training_rewarding,
                                    'training_length': self.conf.training_length
                                    }
                event = json.dumps({'name': 'settings',
                                    'settings': settings
                                    })
                await self.websocket.send(event)

            elif (event['name'] == "new_settings"):
                logging.info('modification of the settings')
                new_conf = event["new_settings"]
                self.configuration_modifier.update_conf(new_conf)

            elif (event['name'] =='device_placing_start'):
                self.device_placing_thread = threading.Thread(target=self.device_placing_module.perform_loop,args=[self, self.device_placing_module.perform])
                self.device_placing_thread.start()
                logging.info('-------------------------------Device Placing Module Start-------------------------------')

            elif (event['name']=='baseline_pre_start'):
                try :
                    self.device_placing_thread.join()
                except :
                    logging.debug('device placing thread already closed')
                finally :
                    self.baseline_module.change_baseline_name('baseline_pre')
                    self.baseline_thread = threading.Thread(target= self.baseline_module.perform_loop,args=[self,self.baseline_module.perform])
                    self.baseline_thread.start()
                    logging.info('-------------------------------Pre Baseline Module Start-------------------------------')

            elif (event['name'] == 'calibration_start'):
                try :
                    self.baseline_thread.join()
                except :
                    logging.debug('baseline thread already closed')                
                finally :
                    self.calibration_thread = threading.Thread(target=self.calibration_module.perform_loop,args=[self, self.calibration_module.perform])
                    self.calibration_thread.start()
                    logging.info('-------------------------------Calibration Module Start-------------------------------')

            elif (event['name'] == 'training_start'):
                try:
                    self.calibration_thread.join()
                except:
                    logging.info('thread for calibration already closed')
                finally:
                    self.training_thread = threading.Thread(target=self.training_module.perform_loop,args=[self, self.training_module.perform])
                    self.training_thread.start()
                    logging.info('-------------------------------Training Module Start-------------------------------')

            elif (event['name'] == 'baseline_post_start'):
                try :
                    self.training_thread.join()
                except :
                    logging.info('thread for training already closed')
                finally :
                    self.baseline_module.change_baseline_name('baseline_post')
                    self.baseline_thread = threading.Thread(target= self.baseline_module.perform_loop,args=[self,self.baseline_module.perform])
                    self.baseline_thread.start()
                    logging.info('-------------------------------Post Baseline Module Start-------------------------------')


            elif (event['name'] == 'request_scores'):
                logging.info('Scores requested')
                try:
                    self.baseline_thread.join()
                except:
                    logging.info('thread for baseline already closed')
                finally:
                    event = json.dumps({'name': 'scores',
                                        'training_score': self.ctx.session_data.training_score,
                                        'noise_score': self.ctx.session_data.noise_score})
                    await self.websocket.send(event)


            elif (event['name'] == "session_end"):
                logging.info('End of the session')
                self.websocket.close()

            elif (event['name'] == "delete_data"):
                self.ctx.session_data_sender.delete_data()
                
            else:
                logging.warning('Incorrect event :{0}'.format(event['name']))
                logging.debug("Incorrect event! Please try again")

    def send_data(self):
        loop = asyncio.new_event_loop()
        asyncio.set_event_loop(loop)
        loop.run_until_complete(self.ctx.session_data_sender.send_data(self.websocket))
        loop.close()
        self.websocket.close()

    def start_session(self):
        session_data = SessionData(self.conf)
        self.ctx.session_data = session_data
        self.recording_dir = self.ctx.session_recorder.start_recording(session_data, self.session_id)
        self.start_logging()

    def start_logging(self):
        logfile_name = self.recording_dir + '{0}_session.log'.format(self.ctx.session_data)
        log_format = '%(asctime)s - %(levelname)s - %(message)s'
        logging.basicConfig(filename=logfile_name, format=log_format)
        logging.info("Recordings are going to be saved in '{0}' folder".format(self.recording_dir))
        logging.info('-------------------------------Logging Start-------------------------------')


