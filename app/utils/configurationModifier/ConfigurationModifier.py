import logging 

class ConfigurationModifier:
    def __init__(self, ctx, conf):
        self.conf = conf
        self.ctx = ctx
        self.updated_conf = None

    def update_conf(self, new_conf):
        self.updated_conf = self.conf.get_copy()
        self._populate_conf_from_new_conf(new_conf)

        self.ctx.configuration_dao.write_configuration(self.updated_conf)
        updated_conf_dict = self.updated_conf.__dict__
        for attribute_name in updated_conf_dict.keys():
            setattr(self.conf, attribute_name, updated_conf_dict[attribute_name])
        self.conf.calculate_dynamic_attributes()

    def _populate_conf_from_new_conf(self, new_conf):
        self.updated_conf.on_head_min_electrode_threshold = new_conf['on_head_min_electrode_threshold']
        self.updated_conf.on_head_max_electrode_threshold = new_conf['on_head_max_electrode_threshold']
        self.updated_conf.negative_electrode_threshold = new_conf['negative_electrode_threshold']
        self.updated_conf.positive_electrode_threshold = new_conf['positive_electrode_threshold']

        self.updated_conf.noise_deviation_threshold = new_conf['noise_deviation_threshold']
        self.updated_conf.training_module_noise_sound_enabled = new_conf['training_module_noise_sound_enabled']
        self.updated_conf.training_rewarding = new_conf['training_rewarding']
        self.updated_conf.training_length = new_conf['training_length']

        logging.info('Configurations updated !')
