import datetime


class DailySessionsRestrictionHandler:
    '''
    Contains logic for handling if session is allowed or not based on the training program schedule
    '''

    def __init__(self, ctx, conf):
        self.ctx = ctx
        self.conf = conf

    def is_session_allowed(self):
        return self._get_current_day() > self.conf.daily_sessions_last_day

    def update_daily_sessions_counter(self):
        self.conf.daily_sessions_counter += 1
        self.conf.daily_sessions_program_completed = False
        if self.conf.daily_sessions_counter >= self.conf.daily_sessions_max_limit:
            self.conf.daily_sessions_counter = 0
            self.conf.daily_sessions_program_completed = True
        self.conf.daily_sessions_last_day = self._get_current_day()
        self.ctx.configuration_dao.write_configuration(self.conf)

    def get_week_number(self):
        return int(self.conf.daily_sessions_counter / 7) + 1

    def is_last_week(self):
        return (self.conf.daily_sessions_max_limit - self.conf.daily_sessions_counter) <= 7

    def get_left_sessions_number(self):
        return 7 - (self.conf.daily_sessions_counter % 7)

    def _get_current_day(self):
        current_date = datetime.datetime.today() - datetime.timedelta(hours=3)
        return current_date.year * 10000 + current_date.month * 100 + current_date.day