from utils.detectors.MuseDeviceOnHeadDetector import MuseDeviceOnHeadDetector
from utils.detectors.MusePhysicalNoiseDetector import MusePhysicalNoiseDetector
from utils.eeg.MuseWindowsEEGSource import MuseWindowsEEGSource
from utils.protocols.MuseBetaProtocol import MuseBetaProtocol
from utils.send_data.WindowsSessionDataSender import WindowsSessionDataSender
from utils.sound.WindowsSoundsBox import WindowsSoundsBox


'''
 Returns objects for Muse on Windows platform
'''
class MuseWindowsObjectFactory:

    def get_EEGSource(self, conf):
        return MuseWindowsEEGSource(conf)

    def get_SoundsBox(self, conf):
        return WindowsSoundsBox(conf)

    def get_DeviceOnHeadDetector(self, conf):
        return MuseDeviceOnHeadDetector(conf)

    def get_PhysicalNoiseDetector(self, conf, eeg_source):
        return MusePhysicalNoiseDetector(conf, eeg_source)

    def get_Protocol(self, conf):
        return MuseBetaProtocol(conf)

    def get_SessionDataSender(self, conf, recording_directory_root):
        return WindowsSessionDataSender(conf, recording_directory_root)
