from utils.detectors.BrainBitDeviceOnHeadDetector import BrainBitDeviceOnHeadDetector
from utils.detectors.BrainBitPhysicalNoiseDetector import BrainBitPhysicalNoiseDetector
from utils.eeg.BrainBitWindowsEEGSource import BrainBitWindowsEEGSource
from utils.protocols.BrainBitThetaProtocol import BrainBitThetaProtocol
from utils.send_data.WindowsSessionDataSender import WindowsSessionDataSender
from utils.sound.WindowsSoundsBox import WindowsSoundsBox


class BrainBitWindowsObjectFactory:

    def get_EEGSource(self, conf):
        return BrainBitWindowsEEGSource(conf)

    def get_SoundsBox(self, conf):
        return WindowsSoundsBox(conf)

    def get_DeviceOnHeadDetector(self, conf):
        return BrainBitDeviceOnHeadDetector(conf)

    def get_PhysicalNoiseDetector(self, conf, eeg_source):
        return BrainBitPhysicalNoiseDetector(conf, eeg_source)

    def get_Protocol(self, conf):
        return BrainBitThetaProtocol(conf)

    def get_SessionDataSender(self, conf, recording_directory_root):
        return WindowsSessionDataSender(conf, recording_directory_root)
