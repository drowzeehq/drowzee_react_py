from utils.detectors.MuseDeviceOnHeadDetector import MuseDeviceOnHeadDetector
from utils.detectors.MusePhysicalNoiseDetector import MusePhysicalNoiseDetector
from utils.eeg.OpenBciWindowsEEGSource import OpenBciWindowsEEGSource
from utils.protocols.OpenBciBandsZscoreProtocol import OpenBciBandsZScoreProtocol
from utils.send_data.WindowsSessionDataSender import WindowsSessionDataSender


'''
 Returns objects for Muse on Windows platform
'''
class OpenBciWindowsObjectFactory:

    def get_EEGSource(self, conf):
        return OpenBciWindowsEEGSource(conf)

    def get_DeviceOnHeadDetector(self, conf):
        return MuseDeviceOnHeadDetector(conf)

    def get_PhysicalNoiseDetector(self, conf, eeg_source):
        return MusePhysicalNoiseDetector(conf, eeg_source)

    def get_Protocol(self, conf):
        return OpenBciBandsZScoreProtocol(conf)

    def get_SessionDataSender(self, conf, recording_directory_root):
        return WindowsSessionDataSender(conf, recording_directory_root)
