import csv
import json
import logging 

from pathlib import Path
import numpy as np


class SessionRecorder:
    def __init__(self, conf, eeg_source, protocol, system_name, recording_directory_root):
        """
        Records data in the next way:

        session_recordings:
        └───<person_name>
            └───session_<timestamp>
                ├───<person_name>_<timestamp>_metrics_placing.csv       electrode noise data
                ├───<person_name>_<timestamp>_raw_placing.csv           raw EEG data
                ├───<person_name>_<timestamp>_raw_baseline_pre.csv         raw EEG data
                ├───<person_name>_<timestamp>_raw_baseline_post.csv         raw EEG data
                ├───<person_name>_<timestamp>_metrics_calibration.csv   noise data
                ├───<person_name>_<timestamp>_raw_calibration.csv       raw EEG data
                ├───<person_name>_<timestamp>_metrics_training.csv      noise data
                ├───<person_name>_<timestamp>_raw_training.csv          raw EEG data
                └───<person_name>_<timestamp>_configurations.json       application configurations

        """

        self.system_name = system_name
        self.recordings_directory = recording_directory_root + 'session_recordings/session_{0}/'
        self.recordings_path = self.recordings_directory + '{1}_{2}'
        self.eeg_source = eeg_source
        self.flush_buffer_size = conf.data_recording_flush_buffer_n_epochs * self.eeg_source.shift_n_samples
        self.raw_data_columns = self.eeg_source.channel_names
        self.device_placing_metrics_columns = ['timestamp']
        self.device_placing_metrics_columns.extend(self.eeg_source.channel_names)
        self.calibration_metric_columns = ['timestamp', 'is_signal_noisy', 'is_eyes_opened']
        self.train_metric_columns = ['timestamp', 'is_signal_noisy', 'is_eyes_opened', 'neurofeedback']
        self.train_metric_columns.extend(
            [threshold_name + '_threshold' for threshold_name in protocol.get_threshold_names()])

    def start_recording(self, session_data, session_uuid):
        self.session_uuid = session_uuid
        recording_dir = Path(self.recordings_directory.format(self.session_uuid))
        recording_dir.mkdir(parents=True, exist_ok=True)
        print("Recordings are going to be saved in '{0}' folder".format(recording_dir))
        self.save_configurations_data(session_data)
        return recording_dir

    def save_configurations_data(self, session_data):
        """Creates file with current session's configurations"""

        filename = self.recordings_path.format(self.session_uuid,
                                               session_data.timestamp,
                                               'configurations.json')
        configurations_dict = session_data.app_configurations.get_all_attributes()
        configurations_dict['calibration_mean_powers'] = session_data.calibration_mean_powers
        configurations_dict['feedback_score'] = session_data.feedback_score
        configurations_dict['feedback_text'] = session_data.feedback_text
        configurations_dict['system_name'] = self.system_name
        with open(filename, 'w') as configurations_file:
            configurations_file.write(json.dumps(configurations_dict, indent=4, separators=(',', ':')))

    def _store_data(self, session_data, epoch, timestamp, metrics, buffer_raw, buffer_metrics):
        """Stores EEG and metrics data into module's buffer"""

        session_data.last_epoch = epoch
        session_data.last_timestamp = timestamp
        raw_data_chunk = epoch[:self.eeg_source.shift_n_samples]
        buffer_raw.extend(raw_data_chunk)
        buffer_metrics.append(metrics)

    def _append_last_epoch(self, session_data, buffer_raw):
        if session_data.last_epoch is not None:
            last_raw_data_chunk = session_data.last_epoch[self.eeg_source.shift_n_samples:]
            buffer_raw.extend(last_raw_data_chunk)

    def resolve_module_file_name(self, session_data, file_type, module_name):
        filename = '{0}_{1}.csv'.format(file_type, module_name)
        return self.recordings_path.format(self.session_uuid,
                                           session_data.timestamp,
                                           filename)

    def _record_module_file(self, session_data, module_name, file_type, data_buffer, columns):
        """Creates file with data from data_buffer"""
        filename = self.resolve_module_file_name(session_data, file_type, module_name)
        write_header = False if Path(filename).exists() else True
        with open(filename, mode='a+', newline='') as data_file:
            data_file_writer = csv.writer(data_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            if write_header:
                data_file_writer.writerow(columns)
            #print(data_buffer)
            data_file_writer.writerows(data_buffer)

    def _flush_raw_data_to_file(self, session_data, buffer_raw, module_name):
        raw_data = np.asarray(buffer_raw).reshape((-1, len(self.eeg_source.channel_names)))
        self._record_module_file(session_data, module_name, 'raw', raw_data, self.raw_data_columns)

        buffer_raw.clear()

    def _flush_metrics_data_to_file(self, session_data, buffer_metrics, module_name, metric_columns):
        metrics_data = np.asarray(buffer_metrics)
        self._record_module_file(session_data, module_name, 'metrics', metrics_data, metric_columns)

        buffer_metrics.clear()

    def store_baseline_data(self, session_data, epoch, timestamp, baseline):

        session_data.last_epoch = epoch
        session_data.last_timestamp = timestamp
        raw_data_chunk = epoch[:self.eeg_source.shift_n_samples]
        session_data.baseline_buffer_raw.extend(raw_data_chunk)
        #print("base buff: ", session_data.baseline_buffer_raw)

        if len(session_data.baseline_buffer_raw) >= self.flush_buffer_size:
            #print("Flush")
            self._flush_raw_data_to_file(session_data, session_data.baseline_buffer_raw, module_name= baseline)

    def save_baseline_data(self, session_data, baseline):
        print("==============================================SAVE=====================================")
        self._append_last_epoch(session_data, session_data.baseline_buffer_raw)
        self._flush_raw_data_to_file(session_data, session_data.calibration_buffer_raw, module_name= baseline)

    def store_device_placing_data(self, session_data, epoch, timestamp, metrics):
        placing_metrics = [timestamp]
        placing_metrics.extend(metrics)
        self._store_data(session_data, epoch, timestamp, placing_metrics, session_data.device_placing_buffer_raw,
                         session_data.device_placing_buffer_metrics)

        if len(session_data.device_placing_buffer_raw) >= self.flush_buffer_size:
            self._flush_raw_data_to_file(session_data, session_data.device_placing_buffer_raw, module_name='placing')

            self._flush_metrics_data_to_file(session_data, session_data.device_placing_buffer_metrics,
                                             module_name='placing',
                                             metric_columns=self.device_placing_metrics_columns)

    def save_device_placing_data(self, session_data):
        self._append_last_epoch(session_data, session_data.device_placing_buffer_raw)
        self._flush_raw_data_to_file(session_data, session_data.device_placing_buffer_raw, module_name='placing')
        self._flush_metrics_data_to_file(session_data, session_data.device_placing_buffer_metrics,
                                         module_name='placing',
                                         metric_columns=self.device_placing_metrics_columns)

    def store_calibration_data(self, session_data, epoch, timestamp, noise_metrics):
        self._store_data(session_data, epoch, timestamp, noise_metrics, session_data.calibration_buffer_raw,
                         session_data.calibration_buffer_metrics)
        if len(session_data.calibration_buffer_raw) >= self.flush_buffer_size:
            self._flush_raw_data_to_file(session_data, session_data.calibration_buffer_raw, module_name='calibration')
            self._flush_metrics_data_to_file(session_data, session_data.calibration_buffer_metrics,
                                             module_name='calibration',
                                             metric_columns=self.calibration_metric_columns)

    def save_calibration_data(self, session_data):
        self._append_last_epoch(session_data, session_data.calibration_buffer_raw)
        self._flush_raw_data_to_file(session_data, session_data.calibration_buffer_raw, module_name='calibration')
        self._flush_metrics_data_to_file(session_data, session_data.calibration_buffer_metrics,
                                     module_name='calibration',
                                     metric_columns=self.calibration_metric_columns)
        self.save_configurations_data(session_data)

    def store_training_data(self, session_data, epoch, timestamp, noise_metrics):
        self._store_data(session_data, epoch, timestamp, noise_metrics, session_data.training_buffer_raw,
                         session_data.training_buffer_metrics)

        if len(session_data.training_buffer_raw) >= self.flush_buffer_size:
            self._flush_raw_data_to_file(session_data, session_data.training_buffer_raw, module_name='training')
            self._flush_metrics_data_to_file(session_data, session_data.training_buffer_metrics, module_name='training',
                                             metric_columns=self.train_metric_columns)

    def save_training_data(self, session_data):
        self._append_last_epoch(session_data, session_data.training_buffer_raw)

        self._flush_raw_data_to_file(session_data, session_data.training_buffer_raw, module_name='training')
        self._flush_metrics_data_to_file(session_data, session_data.training_buffer_metrics, module_name='training',
                                         metric_columns=self.train_metric_columns)
        self.save_configurations_data(session_data)

    def read_training_data(self, metrics_file_path):
        band_values = []
        with open(metrics_file_path) as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            next(csv_reader, None)  # skip the headers
            for row in csv_reader:
                if not int(float(row[1])):
                    band_values.append(float(row[4]))
        return band_values
