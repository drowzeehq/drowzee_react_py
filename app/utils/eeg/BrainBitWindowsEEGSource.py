import time

import numpy as np
import dotnet

assemblypath = r"C:\Program Files\NeuroMD\net-neurosdk-x64.dll"
dotnet.basics.load_assembly(assemblypath)

from Neuro import Device, DeviceInfo, SignalChannel, BatteryChannel, Command
from exceptions.DeviceNotConnectedException import DeviceNotConnectedException


class BrainBitWindowsEEGSource:

    def __init__(self, conf):
        self.conf = conf
        self.sampling_frequency = conf.sampling_frequency

        self.channel_names = ['T3', 'T4', 'O1', 'O2']
        self.channel_indices = [2, 4, 5, 3]
        self.n_channels = len(self.channel_names)

        self.epoch_n_samples = int(conf.epoch_length * self.sampling_frequency)
        self.shift_n_samples = int(conf.shift_length * self.sampling_frequency)

        # deviceInfo.Address = 215671290554607  # 225938017965329

        self.deviceInfo = DeviceInfo()
        self.deviceInfo.Name = 'BrainBit'
        self.device = None

        self.eeg_buffer = np.zeros((int(self.sampling_frequency * conf.eeg_source_buffer_length), self.n_channels))
        self.timestamps_buffer = np.zeros(int(self.sampling_frequency * conf.eeg_source_buffer_length))
        self.bluetooth_enabled = True
        self.connection_established = False
        self.battery_channel = None

    def connect(self, callback):
        if self.device is not None:
            return
        self.deviceInfo.Address = int(self.conf.device_address)
        self.device = Device(self.deviceInfo)
        if self.device is not None:
            self.device.Connect()
            self.channel_info = self.device.Channels
            self.signal_channel = []
            for i in range(2, 6):
                self.signal_channel.append(SignalChannel(self.device, self.channel_info[i]))
            self.battery_channel = BatteryChannel(self.device)
            length = self.signal_channel[0].TotalLength
            while self.signal_channel[0].TotalLength == length:
                self.device.Execute(Command.StartSignal)
            self.connection_established = True

    def disconnect(self):
        if self.device is not None:
            self.device.Disconnect()
            self.device = None

    def get_battery_charge(self):
        while self.battery_channel.TotalLength == 0:
            self.battery_channel.ReadData(0, 1)[0]
        return self.battery_channel.ReadData(0, self.battery_channel.TotalLength)[self.battery_channel.TotalLength - 1]

    def check_bluetooth(self):
        pass

    def _update_buffer(self, new_eeg_data):
        """
        Concatenates "new_eeg_data" into "data_buffer", and returns an array with
        the same size as "data_buffer"
        """

        new_eeg_buffer = np.concatenate((self.eeg_buffer, new_eeg_data), axis=0)
        self.eeg_buffer = new_eeg_buffer[new_eeg_data.shape[0]:, :]

    def next_epoch(self):
        epoch = []
        timestamp = time.time()
        for i in range(len(self.signal_channel)):
            length = self.signal_channel[i].TotalLength
            if length < self.epoch_n_samples:
                eeg_data = self.signal_channel[i].ReadData(0, length)
            else:
                eeg_data = self.signal_channel[i].ReadData(length - self.epoch_n_samples, self.epoch_n_samples)
            eeg_data_array = np.zeros(256)
            for i in range(eeg_data.Length):
                eeg_data_array[i] = eeg_data[i]
            epoch.append(eeg_data_array)
        epoch = np.asarray(epoch).T

        xlen, ylen = epoch.shape[0], epoch.shape[1]
        for x in range(xlen):
            for y in range(ylen):
                epoch[x][y] = epoch[x][y] * 100000
        # self._update_buffer(epoch)
        return epoch, timestamp

    def _check_if_device_connected(self, timestamps):
        if len(timestamps) == 0:
            self.eeg_buffer.fill(0)
            self.timestamps_buffer.fill(0)
            raise DeviceNotConnectedException
