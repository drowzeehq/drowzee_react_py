from kivy.clock import Clock
import numpy as np
import time
import requests
import socket
import json
from threading import Thread, Timer
import logging
from exceptions.DeviceNotConnectedException import DeviceNotConnectedException

last_signals = None
TIMEOUT_RECONNECT = 10
INFO_INTERVAL = 2
PI_API_VERSION = 2


class OpenBciWindowsEEGSource:

    def __init__(self, conf):
        self._init_socket()
        self.conf = conf
        self.sampling_frequency = conf.sampling_frequency

        channel_names = ['TP9', 'AF7', 'AF8', 'TP10']
        self.channel_indices = [i for i in range(conf.n_channels)]
        self.channel_names = [channel_names[i] for i in self.channel_indices]
        self.n_channels = len(self.channel_indices)

        self.epoch_n_samples = int(conf.epoch_length * self.sampling_frequency)
        self.shift_n_samples = int(conf.shift_length * self.sampling_frequency)

        self.eeg_buffer = np.zeros(
            (int(self.sampling_frequency * conf.eeg_source_buffer_length), self.n_channels))
        self.inlet = None
        self.bluetooth_enabled = True
        self.host = "10.0.0.1"
        self.port = 4545
        self.http_port = "4040"

        self.last_sample_time = time.time()
        self.sample_count = 0
        self.broken_packages_count = 0
        self.current_sample_number = 0
        self.last_info_print = time.time()-1

        self.mode = "disconnected"
        # available modes: ["connecting", "disconnected", "sample_mode", "impedance_mode"]
        self.reconnect_timer = False
        self.string_buffer = ""

        self._print_info()

    def connect(self, connected_callback):
        '''
        Connect to the Raspberry device
        :param connected_callback: function to call after the device is connected
        :return:
        '''
        self.connected_callback = connected_callback  # will be called from another function

        # First check if there is any response from HTTP. If not, server is down (no connection)
        try:
            response = self.http_request("/status")
            print('response: ', response)
            logging.info('response : {}'.format(response))
            j = response.json()
            print('Succesfully got response: ', j)
            logging.info('Succesfully got response: {}'.format(j))
            print("PI-API version: ", j["apiVersion"])
            logging.info('PI-API version : {}'.format(j['apiVersion']))
            if j["apiVersion"] != PI_API_VERSION:
                logging.error("Current app not compatible with PI API version: " + str(j["apiVersion"]) + ", looking for version: " + str(PI_API_VERSION))
                return connected_callback(error="Current app not compatible with PI API version: " + str(j["apiVersion"]) + ", looking for version: " + str(PI_API_VERSION))
        except Exception as e:
            logging.debug(e)
            print(e)
            logging.warning("Couldn't connect to " + self.host)
            return connected_callback(error="Couldn't connect to " + self.host)

        if self.mode == "disconnected":
            self.mode = "connecting"
            # we are not connected, create thread and connect
            self.sock.connect((self.host, self.port))
            print('Socket connection Opened')
            logging.info("Socket connection Opened")
            self.thread_reception_data = Thread(target=self._receive_data, args=(self, self.sock,))
            self.thread_reception_data.start()
            response = self.http_request("/search_and_connect")
            print('response: ', response)
            logging.info('response: {}'.format(response))

        else:
            # already a connection up
            # change mode to trigger callback
            self.mode = "connecting"
            print('Connection already open, using existing connection')
            logging.info("Connection already open, using existing connection")

    def http_request(self, endpoint):
        return requests.get("http://"+self.host+":"+self.http_port+endpoint)

    def reconnect(self):
        logging.info('reconnecting')
        print('reconnecting')
        self.sock.close()
        self._init_socket()
        self.mode = "disconnected"
        self.connect(lambda *args: None)

    def disconnect(self):
        logging.info('disconnecting 📀')
        print("disconnecting 📀")
        response = requests.get("http://"+self.host +
                                ":"+self.http_port+"/disconnect")
        print('response: ', response)
        logging.info('response: {}'.format(response))
        self.sock.close()
        self._init_socket()
        self.mode = "disconnected"

        '''
            Not implemented for Windows, as there is no need for it
        '''
        pass

    def get_battery_charge(self):
        '''
            Always returns 100% as for Windows there is no need to know the battery value of device
        '''
        return 100

    def check_bluetooth(self):
        '''
            Not implemented for OpenBci, as the app uses WiFi to receive data
        :return:
        '''
        pass

    def _init_socket(self):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    def _update_buffer(self, new_eeg_data):
        """
        Concatenates "new_eeg_data" into "data_buffer", and returns an array with
        the same size as "data_buffer"
        """

        new_eeg_buffer = np.concatenate(
            (self.eeg_buffer, new_eeg_data), axis=0)
        self.eeg_buffer = new_eeg_buffer[new_eeg_data.shape[0]:, :]

    def _get_last_epoch(self):
        """
        Obtains from "buffer_array" the "newest samples" (N rows from the
        bottom of the buffer) alongside with timestamps
        """
        eeg_data = self.eeg_buffer[(
            self.eeg_buffer.shape[0] - self.epoch_n_samples):, :]
        #print(eeg_data)
        return eeg_data

    def _print_info(self):
        Timer(INFO_INTERVAL, self._print_info).start()
        now = time.time()
        frequency = self.sample_count / (now - self.last_info_print)
        logging.info("🧺 Samples received: {0} 🔥 Frequency: {1} hz.".format(str(self.sample_count),frequency))
        logging.info('🧨 Missing packages: {}'.format(self.broken_packages_count))
        print("🧺 Samples received: "+str(self.sample_count)+" 🔥 Frequency: ",
              frequency, " hz. 🧨 Missing packages: ", self.broken_packages_count)
        self.last_info_print = now
        self.sample_count = 0
        self.broken_packages_count = 0

    @staticmethod
    def _receive_data(self, socket):
        """
        Obtains from "buffer_array" the "newest samples" (N rows from the
        bottom of the buffer) alongside with timestamps
        """
        global last_signals

        self.sock.send("sample_mode".encode())
        last_signals = socket.recv(128).decode()
        while last_signals:
            # print('last_signals: ', last_signals)
            # if self.reconnect_timer:
            #     self.reconnect_timer.cancel()
            # self.reconnect_timer = Timer(TIMEOUT_RECONNECT, self.reconnect)
            # self.reconnect_timer.start()
            last_signals = socket.recv(128).decode()

            process_now = self.string_buffer + last_signals

            rest = self.process_string(process_now)

            self.string_buffer = rest
        print('wah')
        if last_signals == None:
            self.mode = 'disconnected'
            raise DeviceNotConnectedException

    def process_string(self, st):
        if len(st) == 0:
            return ""
        if not st[:1] == "(":
            # broken package?
            first_open_index = st.find("(")
            logging.debug("couldn't make sense of this: {}".format(st[:first_open_index]))
            print("couldn't make sense of this: ", st[:first_open_index])
            return self.process_string(st[first_open_index:])
        else:
            first_end_index = st.find(")")
            if first_end_index == -1:
                # print("no end char found, saving for later")
                return st
            else:
                realstring = st[1:first_end_index]
                rest = st[first_end_index+1:]

                # print("realstring", realstring)
                current_sample_number = int(realstring[:realstring.find("[")])
                values = self._convert_str_signal_to_arr(realstring)
                self.update_values(current_sample_number, values)

                # print("rest", rest)
                return self.process_string(rest)

    def update_values(self, current_sample_number, values):
        # print('last_values: ', last_values)

        self._update_buffer(values)
        # print(current_sample_number, ",", end = '')
        if not current_sample_number < self.current_sample_number:
            # if current_sample_number - (self.current_sample_number + 1):
                # print("🌉",current_sample_number - (self.current_sample_number + 1))
            self.broken_packages_count += current_sample_number - \
                (self.current_sample_number + 1)

        self.current_sample_number = current_sample_number

        self.last_sample_time = time.time()
        self.sample_count += 1
        if self.mode == "connecting":
            print("connected")
            Clock.schedule_once(lambda x: self.connected_callback(), 1)
            self.mode = "sample_mode"

    def next_epoch(self):

        if time.time() - self.last_sample_time > TIMEOUT_RECONNECT:
            print('we should reconnect')
            logging.debug('we should reconnect')

        return self._get_last_epoch(), self.last_sample_time

    def next_epoch_check_nan(self):
        '''
        For Windows acts the same as next_epoch method
        '''
        return self.next_epoch()

    def _convert_str_signal_to_arr(self, last_epoch):
        signals = last_epoch[last_epoch.find("[")+1:-1]
        values = signals.split(',')
        #print('values: ', values)

        try:
            res = np.array([[float(values[0])*100000, float(values[1])*100000, float(values[0])*100000, float(values[1])*100000]])
        except ValueError as e:
            logging.debug('⚠️ Error when parsing to float')
            logging.error(e)
            print('⚠️ Error when parsing to float', e)
            res = np.array([0,0,0,0])
        return res
        # eeg_signals = []
        # for signals in epoch:
        #     signals = signals.replace('[', '').replace(']', '')
        #     values = signals.split(',')
        #     if len(values) != self.n_channels :
        #         values = ['0','0','0','0']
        #     eeg_signals.append(values)
        # xlen, ylen = len(eeg_signals),len(eeg_signals[0])
        # for i in range (xlen):
        #     for j in range(ylen):
        #         eeg_signals[i][j] = float(eeg_signals[i][j])*1000
        # return np.array(eeg_signals)
