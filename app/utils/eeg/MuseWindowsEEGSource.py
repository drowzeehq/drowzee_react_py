from kivy.clock import Clock
from pylsl import StreamInlet, resolve_byprop
import numpy as np
import time

from exceptions.DeviceNotConnectedException import DeviceNotConnectedException


class MuseWindowsEEGSource:

    def __init__(self, conf):
        self.conf = conf
        self.sampling_frequency = conf.sampling_frequency

        channel_names = ['TP9', 'AF7', 'AF8', 'TP10']
        self.channel_indices = [i for i in range(conf.n_channels)]
        self.channel_names = [channel_names[i] for i in self.channel_indices]
        self.n_channels = len(self.channel_indices)

        self.epoch_n_samples = int(conf.epoch_length * self.sampling_frequency)
        self.shift_n_samples = int(conf.shift_length * self.sampling_frequency)

        self.eeg_buffer = np.zeros((int(self.sampling_frequency * conf.eeg_source_buffer_length), self.n_channels))
        self.inlet = None
        self.bluetooth_enabled = True

    def connect(self, connected_callback):
        '''
        Connect to Muse device
        :param connected_callback: function to call after the device is connected
        :return:
        '''
        if self.inlet is not None:
            return

        print('Looking for an EEG stream...')
        streams = resolve_byprop('type', 'EEG', timeout=2)
        if len(streams) == 0:
            raise DeviceNotConnectedException('Device not connected')
        self.inlet = StreamInlet(streams[0], max_chunklen=12)
        stream_info = self.inlet.info()
        self.description = stream_info.desc()
        Clock.schedule_once(connected_callback, 1)


    def disconnect(self):
        '''
            Not implemented for Windows, as there is no need for it
        '''
        pass

    def get_battery_charge(self):
        '''
            Always returns 100% as for Windows there is no need to know the battery value of device
        '''
        return 100

    def check_bluetooth(self):
        '''
            Not implemented for windows, as bluetooth can be checked in system settings
        :return:
        '''
        pass

    def _update_buffer(self, new_eeg_data):
        """
        Concatenates "new_eeg_data" into "data_buffer", and returns an array with
        the same size as "data_buffer"
        """

        new_eeg_buffer = np.concatenate((self.eeg_buffer, new_eeg_data), axis=0)
        self.eeg_buffer = new_eeg_buffer[new_eeg_data.shape[0]:, :]

    def _get_last_epoch(self):
        """
        Obtains from "buffer_array" the "newest samples" (N rows from the
        bottom of the buffer) alongside with timestamps
        """
        eeg_data = self.eeg_buffer[(self.eeg_buffer.shape[0] - self.epoch_n_samples):, :]
        return eeg_data

    def next_epoch(self):
        timestamp = time.time()
        eeg_data = self.pull_chunk(self.conf.epoch_length, self.shift_n_samples)
        self._update_buffer(eeg_data)
        return self._get_last_epoch(), timestamp

    def next_epoch_check_nan(self):
        '''
        For Windows acts the same as next_epoch method
        '''
        return self.next_epoch()

    def pull_chunk(self, chunk_length, n_samples):
        eeg_data, _ = self.inlet.pull_chunk(timeout=chunk_length, max_samples=n_samples)
        self._check_if_device_connected(eeg_data)
        return np.array(eeg_data)[:, self.channel_indices]

    def _check_if_device_connected(self, eeg_data):
        if len(eeg_data) == 0:
            self.eeg_buffer.fill(0)
            raise DeviceNotConnectedException('Device not connected')
